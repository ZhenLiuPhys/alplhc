// jet_reco.cc    (BAT  spring 2014)
//
// Reconstructions for displacement studies
//
//


////  NOTE:  CURRENTLY TREATING ID=36 AS "INVISIBLE", FOR TESTS OF DECAYS OF THE HIGGS BOSON INTO LONG-LIVED SCALARS


#include "displacedlib.h"
#include "fastjet/PseudoJet.hh"
#include "fastjet/ClusterSequence.hh"
#include<iostream> // needed for io
#include<iomanip>
#include<fstream>  // file io
#include<sstream>  // needed for internal io
#include<vector> 

using namespace std;



bool writeVertexFile = false;   // writes a data file containing a list of vertex/cluster information, to compare to CMS histograms
bool onlyCheckCHAMP = false;   // skip full reco and just check CHAMP rates

bool cutoff_bc = true;        // ignores Lxy less than 1 cm for heavy-flavor jets (assuming that any secondary decay b/c-quark displacements might be resolved)
bool cutoffLxy = false;       // ignores Lxy less than 1 cm for all jets (e.g., to fake behavior for a b-enriched sample)

// if both of the following are active, only one particle will be extended/shortened
bool extendOneDecayLength = false;  // artificially extend the life of one of the particles, to improve MC statistics for very low lifetimes
double minTransverseDecayLength = 0.5e-3;  // minimum transverse decay length for one particle, if the above flag is activated
bool shortenOneDecayLength = false;  // artificially decrease the life of one of the particles, to improve MC statistics for very long lifetimes
double maxTransverseDecayLength = 0.60;  // maximum transverse decay length for one particle, if the above flag is activated

// use ATLAS timing cuts (default=true)
bool useTimingCuts = true;


/////////////////////////////////////////////////////////////////////
// GLOBAL PARAMETERS

const double Rjet = 0.5;
const double ptMinJet = 20;      // inclusive jet threshold
const double ptMinRecoJet = 60;  // pt threshold for CMS displaced dijet jets
const double etaMaxJet = 2;
const double HTmin = 320;        // CMS uses 300, but efficiency turn-on is a little slow (see Zuranksi talk)
const double IP3Dmin = 300e-6;   //  *** all distance units are meters
const double IP2Dmin = 500e-6;
const double vzRMS = 0.05;       // RMS of primary vertex location along beampipe

double lifetime;  // seconds

// detector outer boundaries (for CHAMP analysis of CMS 1305.0491)
const double rOuter =  8.0;
const double zEnd = 10.0;
const double etaMaxCHAMP = 2.1;
const double betaMaxCHAMP = 1/1.225;  // beta cut for nominal CMS tracker+TOF analysis
const double betaMaxCHAMP_chargeSuppressed = 0.7;  // an ad hoc cut on velocity for the tracker-only charge suppressed, to encourage large dE/dx and discourage huge time dilations
const double ptMinCHAMP = 70;
const double ptMinCHAMPmu = 80;  // muon-only analysis
const double metMinCHAMP = 150;  // for analysis where R-hadrons are all charge-stripped by muon chambers


// optionally histogram of all Lxy's, for use in calibration
bool LxyHisto = false;
const int    LxyHistoNbins = 11;
const double LxyHistoMin = 0;
const double LxyHistoMax = 0.55;
const double LxyHistoBinWidth = (LxyHistoMax-LxyHistoMin)/LxyHistoNbins;
double LxyHistoEntries[LxyHistoNbins];
double LxyAvg = 0;


//////////////////////////////////////////////////////////////////////
// FUNCTION DECLARATIONS


// write output
void writeOutput(ofstream &, int, int, float, const vector<fastjet::PseudoJet> &,
                 const vector<fastjet::PseudoJet> &, const vector<fastjet::PseudoJet> &,
		 const vector<fastjet::PseudoJet> &, const vector<double> &);

///////////////////////////////////////////////////////////////////////


bool isInvisible(int id)
{
  return (abs(id)==12 || abs(id)==14 || abs(id)==16 || id==1000022 || id==1000039 || id==1000049 || id==36);
}

double chargedRhadronProb(int id)
{
  int absid = abs(id);
  if (absid==1000006)  return 0.56;  // from Pythia8
  if (absid==1000021)  return 0.40;
  return 0;
}


// CMS p.d.f.'s for constructing the likelihood discriminator (extracted and normalized from Fig 1 of EXO-12-038)
const int nBinsVertexTrackMultiplicity = 15;
const double minVertexTrackMultiplicity = 1.5;
const double maxVertexTrackMultiplicity = 16.5;
double vertexTrackMultiplicityPDF_B[nBinsVertexTrackMultiplicity] = {0.549881,0.262598,0.0997873,0.0449043,0.0246842,0.0108716,0.00370263,0.0025472,0.000525196,0.000498936,0,0,0,0,0};
double vertexTrackMultiplicityPDF_S[nBinsVertexTrackMultiplicity] = {0.00751109,0.0247094,0.0441399,0.0620542,0.0782136,0.081555,0.0875498,0.0839277,0.0790279,0.0768378,0.0647639,0.0565227,0.0479587,0.0408407,0.164388};
//
const int nBinsPositiveIPfraction = 10;
const double minPositiveIPfraction = 0;
const double maxPositiveIPfraction = 1.0001;
double positiveIPfractionPDF_B[nBinsPositiveIPfraction] = {0.162956,0.00547531,0.00995985,0.0696146,0.305314,0.0211712,0.119153,0.0479741,0.00547531,0.252907};
double positiveIPfractionPDF_S[nBinsPositiveIPfraction] = {0.00164622,0.00109379,0.00155623,0.00175006,0.00448869,0.00400271,0.00805804,0.013707,0.033769,0.929928};
//
const int nBinsClusterTrackMultiplicity = 15;
const double minClusterTrackMultiplicity = 1.5;
const double maxClusterTrackMultiplicity = 16.5;
double clusterTrackMultiplicityPDF_B[nBinsClusterTrackMultiplicity] = {0.755441,0.164204,0.0494432,0.0185282,0.0064016,0.00369523,0.0017175,0.000346103,0.000223796,0,0,0,0,0,0};
double clusterTrackMultiplicityPDF_S[nBinsClusterTrackMultiplicity] = {0.129262,0.120988,0.117526,0.106207,0.0979337,0.0798431,0.0640887,0.0558429,0.0440653,0.0318427,0.030341,0.0235831,0.0199538,0.0144613,0.0640609};
//
const int nBinsClusterRMS = 10;
const double minClusterRMS = 0;
const double maxClusterRMS = 1.0001;
double clusterRMSPDF_B[nBinsClusterRMS] = {0.112362,0.0880881,0.0733233,0.0648148,0.0688188,0.0733233,0.0828328,0.105856,0.123373,0.207207};
double clusterRMSPDF_S[nBinsClusterRMS] = {0.758731,0.15141,0.0441072,0.0174625,0.00953742,0.00662701,0.00315363,0.00196487,0.00127211,0.00573475};







//////////////////////////////////////////////////////////////////////////
// MAIN

int main (int argc, char ** argv)
{

  // reset global parameters from toplib, if desired
  


  srand(11232698);

  // directories to look for input data files and place output data files
  string inputDir  = "data";////"/Users/brock/thPol/MG5/tt_l+jets_constrained/Events";
  string outputDir = "jets";

  // simulation specifics:
  // tree    = physics process
  // sim     = simulation with specific parameters (e.g., pt bins for same physics process)
  // usertag = additional qualifier for the simulation, appearing at the end of the label
  const int nTrees = 1;
  string physics[nTrees] = {"ttbar.test"};//{"qqbar-ttbar.LHC14.m2500.01"};
  int nSims[nTrees] = {1};
  string sims[nTrees][1] = {{""}};
  string usertag[nTrees] = {""};

  // override with command-line input
  if (nTrees == 1 && argc >= 5) {
    inputDir = argv[1];
    outputDir = argv[2];
    lifetime = atof(argv[3])/speed_of_light; //added input for lifetime in unit of meters
    physics[0] = argv[4];
  }
  if (nTrees == 1 && argc >= 6) {
    usertag[0] = argv[5];
  }

  // iterate over trees  (different physics processes)
  for (int tr = 0; tr < nTrees; tr++) {

    // iterate over simulations  (e.g., pt bins for same physics process)
    for (int simIt = 0; simIt < nSims[tr]; simIt++) {
      string fileKey = physics[tr]+"."+sims[tr][simIt];
      if (sims[tr][simIt] == "")  fileKey = physics[tr];
      string filename;
      
      cout << endl;
      cout << "//////////////////////////////////////////////////////////////////////" << endl;
      cout << "// Simulation " << fileKey << endl;
      cout << "//     (lifetime = " << lifetime*speed_of_light << ")" << endl;
      cout << "// " << endl;
      //cout << endl;
      
      // output files of reconstructed and filtered data
      ofstream jetFile;
      filename = outputDir+"/jets."+fileKey+"."+usertag[tr]+".dat";
      if (usertag[tr] == "") filename = outputDir+"/jets."+fileKey+".dat";
      jetFile.open(filename.c_str());
      
      // output files for vertex properties (to compare to CMS) 
      ofstream vertexFile;
      filename = outputDir+"/vertices."+fileKey+"."+usertag[tr]+".dat";
      if (usertag[tr] == "") filename = outputDir+"/vertices."+fileKey+".dat";
      vertexFile.open(filename.c_str());

      // output files for parton-level vertex properties (for getting denominators to compare to CMS) 
      ofstream partonVertexFile;
      filename = outputDir+"/verticesParton."+fileKey+"."+usertag[tr]+".dat";
      if (usertag[tr] == "") filename = outputDir+"/verticesParton."+fileKey+".dat";
      partonVertexFile.open(filename.c_str());

      // output files of condensed efficiency numbers (for Zhen's plots)
      ofstream effFile;
      filename = outputDir+"/eff."+fileKey+"."+usertag[tr]+".dat";
      if (usertag[tr] == "") filename = outputDir+"/eff."+fileKey+".dat";
      effFile.open(filename.c_str(),ofstream::app);

      // variables characterizing each data file
      double sigma;
      int nGen, nPass;
      
      // event variables
      int eventNum,generatorChannel;
      int nGod,nB,nPart;
      
      // particle variables
      int id;
      double px, py, pz, e;
      int code;
      
      // counters for events loaded/passed
      int nEventsGenerated = 0;
      int nEventsLoaded = 0;
      int nEventsFinal = 0;
      int nAccepted = 0;  // for computing acceptance in CMS's H0 -> X0 X0 model (operates on parton-level info)
      double nPairsPassLow=0;   // number of jet-pairs that pass Low-Lxy criteria
      double nPairsPassHigh=0;  // number of jet-pairs that pass High-Lxy criteria
      
      double sumW2Low=0;  // sum of squares of event weights (to estimate MC errors)
      double sumW2High=0;

      int nZeroPassHigh = 0;
      int nOnePassHigh = 0;
      int nTwoPassHigh = 0;

      // event counters for CMS displaced dilepton analysis
      int nEventsPassDisplacedDilepton = 0;

      // event counters for CMS displaced e+mu analysis
      double nEventsPassDisplacedElectronMuonSR1 = 0;
      double nEventsPassDisplacedElectronMuonSR2 = 0;
      double nEventsPassDisplacedElectronMuonSR3 = 0;

      // track counters for CHAMP analysis
      int nCHAMPfiducial = 0;
      int nCHAMPfiducialMET = 0;
      int nCHAMPoutsideDetector = 0;
      int nCHAMPgood = 0;
      int nCHAMPgoodMET = 0;

      // event counters for ATLAS muon spectrometer analysis
      double nEventsATLASmuon = 0;  // weighted # events
      int nMCeventsATLASmuon = 0;  // actual # MC events

      // event counters for ATLAS HCAL analysis
      double nEventsATLASHCAL = 0;  // weighted # events
      int nMCeventsATLASHCAL = 0;  // actual # MC events
      double nEventsATLASHCAL_loose = 0;  // with weaker cuts
      int nMCeventsATLASHCAL_loose = 0;

      // displaced vertex counter for ATLAS displaced muon+tracks analysis
      double nATLASmuonDV = 0;
      double nATLASmuonDVlite = 0;  // loosening the criterion that a muon is matched to the vertex

      double sigmaSum = 0;

      // First check if there are multiple files
      int nFiles = 0;
      bool multipleFiles = true;  // default: look for multiple files labelled 01, 02, etc
      string fileName;
      char fileNum[100];
      string datFileNames[100];
      while (1) {
	
	nFiles++;
	
	ifstream datfileTemp;
	if (multipleFiles) {
	  if (nFiles < 10)   sprintf(fileNum,"0%i",nFiles);
	  else               sprintf(fileNum,"%i" ,nFiles);
	}
	fileName = inputDir+"/output."+fileKey+"."+fileNum+".dat";
	datfileTemp.open(fileName.c_str());
	if(!datfileTemp.good() && nFiles == 1)  {
	  multipleFiles = false;
	  fileName = inputDir+"/output."+fileKey+".dat";
	}
	datFileNames[nFiles-1] = fileName;
	
	ifstream sigfile;
	if (!multipleFiles)  fileName = inputDir+"/sigma."+fileKey+".dat";
	else                 fileName = inputDir+"/sigma."+fileKey+"."+fileNum+".dat";
	sigfile.open(fileName.c_str());
	if (!sigfile.good())  break;
	sigfile >> sigma >> nGen >> nPass;
	//cout << "Checking sigma file #" << nFiles << endl;
	//cout << nGen << "   generated, " << nPass << " passed" << endl << endl;
	nEventsGenerated += nGen;
	sigmaSum += sigma;
	sigfile.close();
	
	if (!multipleFiles) break;
      }
      if (multipleFiles) nFiles--; // overcounted on last file check
      double sigmaAvg = sigmaSum/nFiles;   // assuming files contain unweighted events
      

      // Read input files
      for (int fileIt = 0; fileIt < nFiles; fileIt++) {
	
	ifstream datfile;
	datfile.open(datFileNames[fileIt].c_str());
	cout << datFileNames[fileIt] << endl;
	if(!datfile.good())  {
	  cout << "FAILING TO FIND:  " << datFileNames[fileIt] << endl;
	  break;
	}
	//cout << endl << "------------------------------------------------------" << endl;
	//cout << "Open file #" << fileIt+1 << endl;
	
	// iterate over events in the data file
	while (1) {

	  
	  // counters for this specific event
	  int nPairsPassHighThisEvent = 0;

	  // "truth" particle holders
	  vector<fastjet::PseudoJet> godParticles;
	  vector<fastjet::PseudoJet> b_hadrons;

	  // the displaced hard particles (identified by codes after their 4-vectors in the "output" file)
	  const int nDisplacedMax = 100;  // some big # of slots reserved
	  int nDisplaced = 0;  // actual # found in file
	  vector<fastjet::PseudoJet> displacedHardParticles;
	  vector< vector<fastjet::PseudoJet> >  displacedHardParticleProducts;
	  for (int i = 0; i < nDisplacedMax; i++) {
	    displacedHardParticles.push_back(blankJet);
	    vector<fastjet::PseudoJet> temp;
	    displacedHardParticleProducts.push_back(temp);
	  }
	  double displacedWeight = 1.;  // an extra event weight in case we restrict the decay phase space

	  // final-state particle/jet holders
	  vector<fastjet::PseudoJet> leptons;
	  vector<fastjet::PseudoJet> isoLeptons;  // will ultimately add in mini-isolated
	  vector<fastjet::PseudoJet> miniIsoLeptons;
	  vector<fastjet::PseudoJet> invisibles;  // neutrinos & neutralinos
	  vector<fastjet::PseudoJet> hadrons;
	  vector<fastjet::PseudoJet> jets;
	  vector<double> extras;
	  
	  // (displaced) vertices (0 = primary)
	  fastjet::PseudoJet vertices[nDisplacedMax];

	  //  partons from pseudoscalar decay (if applicable, and only for checking against CMS)
	  fastjet::PseudoJet q1a,q1b,q2a,q2b;
	  int nq = 0;

	  float CKKWweight = 0;

	  // flag for skipping the event...use as desired
	  bool skipEvent = false;

	  // flags to check if the displaced particles are squarks or gluinos, information that may be used to determine R-hadronization charged/neutral fractions.
	  // **note that this is ridiculously primitive because the hard event output as currently programmed does not specify which hard particles are displaced.  
	  // so for now we just do the following.  tracing through the hard event, look at the parent ids (appearing immediately after the 4-vector).  record the
	  // last sparticle appearing as a parent.  if it is a squark, assume a long-lived squark pair.  if it is a gluino, assume a long-lived gluino pair.
	  // (in events where, e.g., a squark decays into a long-lived neutralino, the neutralino daughters will appear last, and show the neutralino as
	  // their parent.)  it goes without saying that this will not catch more exotic options, such as displaced color-sextets or whatever, or generally
	  // any colored displaced particles outside of a minimal SUSY context.
	  int id_lastColoredSparticleParent = 0;	  
	  bool displacedGluinos = false;
	  bool displacedSquarks = false;


	  ///////////////////////////////
	  // Read event from input file
	  ///////////////////////////////
	  
	  // load God-level info
	  string line;
	  getline(datfile,line);
	  if (!datfile.good()) break;
	  //cout << line << endl;
	  int hasWeight = sscanf(line.c_str(),"%*d %*d %*f")+1;  // check if reweighted event (e.g., from CKKW-L)
	  if (hasWeight)  sscanf(line.c_str(),"%d %d %f",&eventNum,&nGod,&CKKWweight);
	  else  sscanf(line.c_str(),"%d %d",&eventNum,&nGod);
	  for (int i = 0; i < nGod; i++) {
	    datfile >> id >> px >> py >> pz >> e >> code;
	    fastjet::PseudoJet part(px,py,pz,e);
	    part.set_user_index(id);
	    godParticles.push_back(part);
	    if (abs(code) > 1e6 && abs(code) < 3e6)  id_lastColoredSparticleParent = code;
	    if (i > 1 && abs(id) <= 5) {
	      if (nq==0)  q1a = part;
	      if (nq==1)  q1b = part;
	      if (nq==2)  q2a = part;
	      if (nq==3)  q2b = part;
	      nq++;
	    }
	  }
	  if (abs(id_lastColoredSparticleParent) >= 1000001 && abs(id_lastColoredSparticleParent) <= 1000006 ||
	      abs(id_lastColoredSparticleParent) >= 2000001 && abs(id_lastColoredSparticleParent) <= 2000006   )  displacedSquarks = true;
	  if (id_lastColoredSparticleParent == 1000021)  displacedGluinos = true;

	  // load b hadrons
	  datfile >> id >> nB;
	  for (int i = 0; i < nB; i++) {
	    datfile >> id >> px >> py >> pz >> e;
	    b_hadrons.push_back(fastjet::PseudoJet(px,py,pz,e));
	    b_hadrons.back().set_user_index(id);
	  }

	  // load final state particles
	  vector<fastjet::PseudoJet> allFinalParticles;
	  datfile >> id >> nPart;	
	  for (int i = 0; i < nPart; i++) {
	    datfile >> id >> px >> py >> pz >> e >> code;
	    fastjet::PseudoJet part(px,py,pz,e);
	    part.set_user_index(id);
	    part.set_user_info(new ParticleInfo(code,vertices[0]));
	    allFinalParticles.push_back(part);
	    if (code>0)  { displacedHardParticles[code] += part;  displacedHardParticleProducts[code].push_back(part); }
	    if (code>nDisplaced)  nDisplaced=code;
	  }

	  // simple acceptance check (only valid for H0 -> X0 X0 model, don't use these vertices later!!!)
	  if (nq==4) {
	    fastjet::PseudoJet A1 = q1a+q1b;
	    fastjet::PseudoJet A2 = q2a+q2b;
	    fastjet::PseudoJet vertexA1 = lifetime*Exponential() * A1/A1.m() * speed_of_light;
	    fastjet::PseudoJet vertexA2 = lifetime*Exponential() * A2/A2.m() * speed_of_light;
	    if (q1a.pt()>40 && q1b.pt()>40 && fabs(q1a.eta())<2.1 && fabs(q1b.eta())<2.1 && DR(q1a,q1b)>0.5 && vertexA1.perp()<0.60)  nAccepted++;
	    if (q2a.pt()>40 && q2b.pt()>40 && fabs(q2a.eta())<2.1 && fabs(q2b.eta())<2.1 && DR(q2a,q2b)>0.5 && vertexA2.perp()<0.60)  nAccepted++;
	    //cout << (A1+A2).m() << "  ->  " << A1.m() << "  " << A2.m() << endl;
	    /////cout << 180./Pi*acos( (q1a.px()*q1b.px()+q1a.py()*q1b.py()+q1a.pz()*q1b.pz())/pTot(q1a)/pTot(q1b) ) << endl;
	  }

	  // compute displaced vertices
	  vertices[0] = blankJet;  // initialize primary vertex
	  int restrictedDecayCode = -1;
	  if (extendOneDecayLength || shortenOneDecayLength)  restrictedDecayCode = int(1 + 0.999999999*uniformRandom()*nDisplaced);
	  for (int i = 1; i < nDisplacedMax; i++) {
	    if (displacedHardParticles[i].e() <= 0)  continue;
	    double properTimeMin = 0;
	    double properTimeMax = lifetime*1e6;
	    if (i==restrictedDecayCode) {
	      if (extendOneDecayLength)
		properTimeMin = minTransverseDecayLength * displacedHardParticles[i].m() / displacedHardParticles[i].pt() / speed_of_light;
	      if (shortenOneDecayLength)
		properTimeMax = maxTransverseDecayLength * displacedHardParticles[i].m() / displacedHardParticles[i].pt() / speed_of_light;
	      if (properTimeMax < properTimeMin)  { properTimeMin = 0;  properTimeMax = lifetime*1e6; }  // error trap
	      double displacedProb = exp(-properTimeMin/lifetime) - exp(-properTimeMax/lifetime);
	      displacedWeight *=  2*displacedProb - displacedProb*displacedProb;   // note the measure here:  we're restricting to two "strips" in the (time1,time2) plane, subtracting double-counting from the overlap....if I don't do this, empirically I can get rates that are 2x too small! ... note that this is specialized to the case of *TWO* displaced particles
	    }
	    //vertices[i] = (properTimeMin + lifetime*Exponential()) * displacedHardParticles[i]/displacedHardParticles[i].m() * speed_of_light;
	    vertices[i] = lifetime*RestrictedExponential(properTimeMin/lifetime,properTimeMax/lifetime) * displacedHardParticles[i]/displacedHardParticles[i].m() * speed_of_light;
	    double rad = vertices[i].perp();
	    if (LxyHisto && i <= 2) {
	      int bin = (rad-LxyHistoMin)/LxyHistoBinWidth;
	      if (bin >= 0 && bin < LxyHistoNbins)  LxyHistoEntries[bin]++;
	      LxyAvg += rad;
	    }
	  }

	  // introduce beamspot spread
	  zPrimaryVertex = vzRMS*Normal();
	  for (int i = 0; i < nDisplacedMax; i++)
	    vertices[i].reset(vertices[i].px(),vertices[i].py(),vertices[i].pz()+zPrimaryVertex,vertices[i].e());
	  
	  // reset final state particle vertices, distribute amongst containers
	  for (int i = 0; i < nPart; i++) {
	    fastjet::PseudoJet part = allFinalParticles[i];
	    id = part.user_index();
	    code = part.user_info<ParticleInfo>().displacedCode();
	    ((ParticleInfo*)part.user_info_ptr())->resetVertex(vertices[code]);
	    if (part.perp() < 0.01)  continue;  // ditch extremely low-pt to avoid DR calculation errors
	    if ((abs(id)==11 || abs(id)==13) && fabs(part.eta()) < etaMaxTrack && part.perp() > ptMinLep)
	      leptons.push_back(part);
	    else if (isInvisible(id))
	      invisibles.push_back(part);
	    else
	      hadrons.push_back(part);
	  }

	  // if desired, write information about this displaced dijet candidate to file for study in ROOT (read by CMSplots.C)
	  for (int i = 1; i < nDisplacedMax && writeVertexFile; i++) {
	    //if (displacedHardParticles[i].e() > 0)
	      // engineer some of the output to be fake so that automatically passes into CMSplots analysis (see vertexFile write below for details on these variables)
	      //////partonVertexFile << "16 1 16 0.001 1. " << vertices[i].perp() << " 0 0 0 0 " << displacedHardParticles[i].perp() << endl; 
	  }
  
	  
	  // increment getlines to set up next event read
	  getline(datfile,line);
	  getline(datfile,line);
	  
	  if (skipEvent)  continue;
	  

	  nEventsLoaded++;
	  if (nEventsLoaded % 1000 == 0) {
	    cout << eventNum << endl;
	  }


	  ///////////////////////////////////////////////////////////////////////////////////
	  // Check if displaced particles are candidates for CMS CHAMP searches (1305.3491)
	  ///////////////////////////////////////////////////////////////////////////////////

	  // The nominal analysis is specialized to the tracker+TOF, so includes a cutoff on beta.  I also include the MET-triggered subset of the 
	  // tracker-only analysis, which is specialized to the extreme case where all CHAMPS are charged-stripped by the time they get to the muon 
	  // system.  It exploits the fact that the CHAMP tracks would then get ignored by particle-flow at trigger-level, and would look like MET.
	  // Such events are assumed to pass only if *both* long-lived particles decay outside the detector.  For each of candidate I also include
	  // a maximum beta, to ensure that dE/dx has a reasonable chance of being high and to suppress short-lived events with huge time dilations.
	  // (I.e., starting to become a "muon"!)

	  bool hardRecoil = (displacedHardParticles.size() >= 2 && (displacedHardParticles[1]+displacedHardParticles[2]).pt() > metMinCHAMP);
	  bool allOutsideDetector = true;
	  int nCHAMPgoodMET_local = 0;
	  for (int i = 1; i <= 2; i++) {  // only look for up to 2 candidates (hardcoded for pair production)
	    if (displacedHardParticleProducts[i].size() == 0)  continue;
	    bool fiducial = (fabs(displacedHardParticles[i].eta()) < etaMaxCHAMP && displacedHardParticles[i].pt() > ptMinCHAMP);  // low-eta, high-pt
	    double beta = pTot(displacedHardParticles[i])/displacedHardParticles[i].E();
	    bool lowBeta = (pTot(displacedHardParticles[i])/displacedHardParticles[i].E() < betaMaxCHAMP);
	    if (fiducial && lowBeta)  nCHAMPfiducial++;
	    if (fiducial && hardRecoil)  nCHAMPfiducialMET++; // count separately events where the diCHAMP system has a lot of recoil (they can be picked up as "MET")
	    bool outsideDetector = (vertices[i].perp() > rOuter || fabs(vertices[i].pz()) > zEnd);
	    if (fiducial && outsideDetector)  nCHAMPoutsideDetector++;  // raw count of fiducial R-hadron candidates that decay outside detector...
	    double vx = vertices[i].px();
	    double vy = vertices[i].py();
	    double vz = vertices[i].pz();
	    double vT2 = vx*vx+vy*vy;
	    for (int j = 0; j < displacedHardParticleProducts[i].size(); j++) {  // ...but to be thorough, check that no decay particles re-enter detector
	      if (isInvisible(displacedHardParticleProducts[i][j].user_index()))  continue;
	      double betax = displacedHardParticleProducts[i][j].px()/displacedHardParticleProducts[i][j].e();
	      double betay = displacedHardParticleProducts[i][j].py()/displacedHardParticleProducts[i][j].e();
	      double betaz = displacedHardParticleProducts[i][j].pz()/displacedHardParticleProducts[i][j].e();	      
	      double betaT2 = betax*betax+betay*betay;
	      double vTdotBetaT = vx*betax+vy*betay;
	      //  particle position vector evolves as r(t) = v + t*beta, where t is time after production (measured here in meters)
	      double tPlus = (zEnd-vz)/betaz;  double tMinus = (-zEnd-vz)/betaz;  // time at which we cross the planes z = +/-zEnd at detector ends
	      if (tPlus < 0 && tMinus < 0)  continue;  // never crosses detector end planes, pass!
	      if (tPlus < 0)  tPlus = 0;   if (tMinus < 0)  tMinus = 0;   // only consider positive times
	      double tHigh = tPlus;  double tLow = tMinus;
	      if (tPlus < tMinus)  { tHigh = tMinus;  tLow = tPlus; }   // establish high and low edges for time window when within detector end planes
	      double r2High = vT2 + tHigh*tHigh*betaT2 + 2*tHigh*vTdotBetaT;
	      double r2Low  = vT2 + tLow *tLow *betaT2 + 2*tLow *vTdotBetaT;
	      double r2Min = min(r2High,r2Low);  // first guess for closest approach: smaller radius of the beginning or end of the time window
	      double tMin = -vTdotBetaT/betaT2;  // time at which we are at global closest approach in radius
	      if (tLow < tMin && tMin < tHigh)  r2Min = vT2 + tMin*tMin*betaT2 + 2*tMin*vTdotBetaT;  // absolute closest approach occurs within the time window
	      if (r2Min < rOuter*rOuter)  { outsideDetector = false;  break; }  // decay products passed through detector volume
	    }
	    if (fiducial && lowBeta && outsideDetector)  nCHAMPgood++;
	    if (fiducial && hardRecoil && beta<betaMaxCHAMP_chargeSuppressed && outsideDetector)  nCHAMPgoodMET_local++;	    
	    allOutsideDetector *= outsideDetector;
	  }
	  if (allOutsideDetector)  nCHAMPgoodMET += nCHAMPgoodMET_local;


	  if (onlyCheckCHAMP)  continue;


	  /////////////////////////////////////////////////////////////////////////////////////////
	  // Check if displaced particles are candidates for ATLAS muon-system search (1203.1303)
	  /////////////////////////////////////////////////////////////////////////////////////////

	  // NOTE: There is great uncertainty here about how this would behave with our signal, since ATLAS's search is designed to look for decays
	  //       of 20/40 GeV displaced particles, not 100's of GeV.  We here give them the benefit of the doubt, and assume that the efficiency
	  //       is not a strong function of mass (-> decay particle multiplicity and energy)...i.e., that their algorithms are not 
	  //       "overwhelmed".  One of the more important "fiducial" cuts here appears to be the timing relative to the trigger.
	  //
	  // NOTE 2:  The actual analysis has an isolation requirement from jets and tracks, which we don't reproduce here.  **Only apply this analysis
	  //          to neutral long-lived particles, including neutral R-hadrons.  Multiply by appropriate neutral hadronization fractions where appropriate.**



	  if (displacedHardParticleProducts[1].size() > 0 && displacedHardParticleProducts[2].size() > 0) {  // only look for up to 2 candidates (hardcoded for pair production)
	    bool passEvent = true;
	    double trigger_efficiency = 0.5;  // success rate for muon trigger to fire within a fiducial region (approximating flat efficiency in that region)
	    double vertex_efficiency = 0.4;  // ~40% reco efficiencies on each vertex (possibly with some extra fudge factors)
	    double weight = vertex_efficiency*vertex_efficiency;
	    double max_delay = 7e-9;  // seconds
	    if (!useTimingCuts)  max_delay = 100000.;
	    double time_of_flight_delay1 =  pTot(vertices[1]-vertices[0]) * (displacedHardParticles[1].e()/pTot(displacedHardParticles[1])-1) / speed_of_light;
	    double time_of_flight_delay2 =  pTot(vertices[2]-vertices[0]) * (displacedHardParticles[2].e()/pTot(displacedHardParticles[2])-1) / speed_of_light;
	    if (time_of_flight_delay1 > max_delay || time_of_flight_delay2 > max_delay)  passEvent = false;  // both decays must occur within DAQ window
	    double eta1 = fabs(displacedHardParticles[1].eta());  double r1 = vertices[1].perp();  double z1 = vertices[1].pz();
	    double eta2 = fabs(displacedHardParticles[2].eta());  double r2 = vertices[2].perp();  double z2 = vertices[2].pz();
	    bool trigger1 = (eta1 < 1.0 && r1 > 4.0 && r1 < 6.5);  // *rough* approximation of regions where trigger is active
	    bool trigger2 = (eta2 < 1.0 && r2 > 4.0 && r2 < 6.5);
	    if (trigger1 && !trigger2  ||  trigger2 && !trigger1)  weight *= trigger_efficiency;  // one chance to trigger
	    if (trigger1 && trigger2)  weight *= 1 - (1-trigger_efficiency)*(1-trigger_efficiency);  // two chances to trigger
	    passEvent *= (trigger1 || trigger2);
	    bool reco1 = (eta1 < 1.0 && r1 > 4.0 && r1 < 7.5  ||  eta1 > 1.0 && eta1 < 2.4 && z1 > 8 && z1 < 14);
	    bool reco2 = (eta2 < 1.0 && r2 > 4.0 && r2 < 7.5  ||  eta2 > 1.0 && eta2 < 2.4 && z2 > 8 && z2 < 14);
	    passEvent *= (reco1 && reco2);
	    double ET_HCAL[3] = {0,0,0};  // one extra entry for the prompt vertex (not used)
	    for (int i = 1; i <= 2; i++) {   // apply extra quality cut:  limit the amount of visible energy that points back to HCAL
	      double vx = vertices[i].px();
	      double vy = vertices[i].py();
	      double vz = vertices[i].pz();
	      double vT2 = vx*vx+vy*vy;
	      for (int j = 0; j < displacedHardParticleProducts[i].size(); j++) { 
		if (isInvisible(displacedHardParticleProducts[i][j].user_index()))  continue;
		double betax = displacedHardParticleProducts[i][j].px()/displacedHardParticleProducts[i][j].e();
		double betay = displacedHardParticleProducts[i][j].py()/displacedHardParticleProducts[i][j].e();
		double betaz = displacedHardParticleProducts[i][j].pz()/displacedHardParticleProducts[i][j].e();	      
		double betaT2 = betax*betax+betay*betay;
		double vTdotBetaT = vx*betax+vy*betay;
		//  particle position vector evolves as r(t) = v + t*beta, where t is time after production (measured here in meters)
		double tPlus = (zOuterHCAL_ATLAS-vz)/betaz;  double tMinus = (-zOuterHCAL_ATLAS-vz)/betaz;  // time at which we cross the planes at HCAL outer ends
		if (tPlus < 0 && tMinus < 0)  continue;  // never crosses detector end planes, pass!
		if (tPlus < 0)  tPlus = 0;   if (tMinus < 0)  tMinus = 0;   // only consider positive times
		double tHigh = tPlus;  double tLow = tMinus;
		if (tPlus < tMinus)  { tHigh = tMinus;  tLow = tPlus; }   // establish high and low edges for time window when within detector end planes
		double r2High = vT2 + tHigh*tHigh*betaT2 + 2*tHigh*vTdotBetaT;
		double r2Low  = vT2 + tLow *tLow *betaT2 + 2*tLow *vTdotBetaT;
		double r2Min = min(r2High,r2Low);  // first guess for closest approach: smaller radius of the beginning or end of the time window
		double tMin = -vTdotBetaT/betaT2;  // time at which we are at global closest approach in radius
		if (tLow < tMin && tMin < tHigh)  r2Min = vT2 + tMin*tMin*betaT2 + 2*tMin*vTdotBetaT;  // absolute closest approach occurs within the time window
		if (r2Min < rOuterHCAL_ATLAS*rOuterHCAL_ATLAS)  { ET_HCAL[i] += displacedHardParticleProducts[i][j].e()*vertices[i].perp()/pTot(vertices[i]); }  // decay products passed through detector volume...roughly approximate deposited ET
	      }
	    }
	    if (ET_HCAL[1] > 15 || ET_HCAL[2] > 15)  passEvent = false;  // veto if either of the candidates *could* fail isolation...
	    
	    if (passEvent)  { nEventsATLASmuon += weight;  nMCeventsATLASmuon++; }
	  }


	  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	  // Check if displaced particles are candidates for ATLAS low-EM jet (HCAL/ECAL >> 1) search (ATLAS-CONF-2014-041)
	  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	  if (displacedHardParticleProducts[1].size() > 0 && displacedHardParticleProducts[2].size() > 0) {  // only look for up to 2 candidates (hardcoded for pair production)   
	    bool passEvent = true;
	    double ETcut1 = 60;  // harder jet
	    double ETcut2 = 40;  // softer jet
	    double reco_efficiency = 0.55;  // calibrated guess for effects of, e.g., ECAL/HCAL cut and other isolation cuts (deconvolved from trigger efficiency)    
	    double weight = reco_efficiency*reco_efficiency;
	    double max_delay = 5e-9;  // seconds (explicit cut in the final analysis)
	    if (!useTimingCuts)  max_delay = 100000.;
	    double ETvis[3] = {0,0,0};  // one extra entry for the prompt vertex (not used)
	    double ET_ECAL[3] = {0,0,0};
	    fastjet::PseudoJet METvector = displacedHardParticles[1]+displacedHardParticles[2];  // initialize MET vector as if the decays were both unseen...accounts for the prompt momentum recoil
	    for (int i = 1; i <= 2; i++) {   // add up visible energy, and check how much energy is pointing back to the ECAL
	      double vx = vertices[i].px();
	      double vy = vertices[i].py();
	      double vz = vertices[i].pz();
	      double vT2 = vx*vx+vy*vy;
	      for (int j = 0; j < displacedHardParticleProducts[i].size(); j++) { 
		if (isInvisible(displacedHardParticleProducts[i][j].user_index()))  continue;
		ETvis[i] += displacedHardParticleProducts[i][j].e()*vertices[i].perp()/pTot(vertices[i]);
		METvector -= displacedHardParticleProducts[i][j].e()*vertices[i]/pTot(vertices[i]);  // we only see the energy, not momentum, pointing toward a hot group of CAL cells
		////METvector -= displacedHardParticleProducts[i][j];  // test version where we add back full 4-momentum (this should always balance if there are no invisibles)
		double betax = displacedHardParticleProducts[i][j].px()/displacedHardParticleProducts[i][j].e();
		double betay = displacedHardParticleProducts[i][j].py()/displacedHardParticleProducts[i][j].e();
		double betaz = displacedHardParticleProducts[i][j].pz()/displacedHardParticleProducts[i][j].e();	      
		double betaT2 = betax*betax+betay*betay;
		double vTdotBetaT = vx*betax+vy*betay;
		//  particle position vector evolves as r(t) = v + t*beta, where t is time after production (measured here in meters)
		double tPlus = (zOuterECAL_ATLAS-vz)/betaz;  double tMinus = (-zOuterECAL_ATLAS-vz)/betaz;  // time at which we cross the planes at ECAL outer ends
		if (tPlus < 0 && tMinus < 0)  continue;  // never crosses detector end planes, pass!
		if (tPlus < 0)  tPlus = 0;   if (tMinus < 0)  tMinus = 0;   // only consider positive times
		double tHigh = tPlus;  double tLow = tMinus;
		if (tPlus < tMinus)  { tHigh = tMinus;  tLow = tPlus; }   // establish high and low edges for time window when within detector end planes
		double r2High = vT2 + tHigh*tHigh*betaT2 + 2*tHigh*vTdotBetaT;
		double r2Low  = vT2 + tLow *tLow *betaT2 + 2*tLow *vTdotBetaT;
		double r2Min = min(r2High,r2Low);  // first guess for closest approach: smaller radius of the beginning or end of the time window
		double tMin = -vTdotBetaT/betaT2;  // time at which we are at global closest approach in radius
		if (tLow < tMin && tMin < tHigh)  r2Min = vT2 + tMin*tMin*betaT2 + 2*tMin*vTdotBetaT;  // absolute closest approach occurs within the time window
		if (r2Min < rOuterECAL_ATLAS*rOuterECAL_ATLAS)  { ET_ECAL[i] += displacedHardParticleProducts[i][j].e()*vertices[i].perp()/pTot(vertices[i]); }  // decay products passed through detector volume...roughly approximate deposited ET
	      }
	    }
	    double ET1 = ETvis[1];
	    double ET2 = ETvis[2];
	    if (!(ET1>ETcut1 && ET2>ETcut2  ||  ET2>ETcut1 && ET1>ETcut2))  passEvent = false;
	    double eta1 = fabs(displacedHardParticles[1].eta());  double r1 = vertices[1].perp();  double z1 = vertices[1].pz();
	    double eta2 = fabs(displacedHardParticles[2].eta());  double r2 = vertices[2].perp();  double z2 = vertices[2].pz();
	    bool in_barrel1 = (eta1 < 1.7 && r1 > 2.25 && r1 < 3.5); // *rough* approximation of trigger-active HCAL volume
	    bool in_barrel2 = (eta2 < 1.7 && r2 > 2.25 && r2 < 3.5);
	    bool in_endcap1 = (eta1 < 2.5 && r1 < 2.0 && z1 > 4.25 && z1 < 5.0);
	    bool in_endcap2 = (eta2 < 2.5 && r2 < 2.0 && z2 > 4.25 && z2 < 5.0);
	    bool in_HCAL1 = in_barrel1 || in_endcap1;
	    bool in_HCAL2 = in_barrel2 || in_endcap2;
	    passEvent *= (in_HCAL1 && in_HCAL2);
	    double time_of_flight_delay1 =  pTot(vertices[1]-vertices[0]) * (displacedHardParticles[1].e()/pTot(displacedHardParticles[1])-1) / speed_of_light;
	    double time_of_flight_delay2 =  pTot(vertices[2]-vertices[0]) * (displacedHardParticles[2].e()/pTot(displacedHardParticles[2])-1) / speed_of_light;
	    passEvent *= (time_of_flight_delay1 < max_delay && time_of_flight_delay2 < max_delay);  // demand both arrive within same trigger window as prompt event
	    bool passEvent_loose = passEvent;
	    if (ET_ECAL[1]/ET1 > 0.  ||  ET_ECAL[2]/ET2 > 0.)  passEvent = false;  // veto if either of the candidates *could* fail isolation...
	    if (METvector.pt() > 50)  { passEvent = false;  passEvent_loose = false; }  // veto if a large amount of MET (either real or apparent) is created
	    if (passEvent)  { nEventsATLASHCAL += weight;  nMCeventsATLASHCAL++; }
	    if (passEvent_loose)  { nEventsATLASHCAL_loose += weight;  nMCeventsATLASHCAL_loose++; }
	  }



	  ///////////////////////////////////////////////////////
	  // Compute MET vector and total HT (for MET smearing)
	  ///////////////////////////////////////////////////////

	  double METx = 0;  double METy = 0;
	  for (int i = 0; i < invisibles.size(); i++)  { METx += invisibles[i].px();  METy += invisibles[i].py(); }
	  extras.push_back(METx);  extras.push_back(METy);
	  double HTfull = 0; // HT of all particles (including pileup) will determine MET smearing downstream
	  for (int i = 0; i < hadrons.size(); i++)  HTfull += hadrons[i].perp(); 
	  for (int i = 0; i < leptons.size(); i++)  HTfull += leptons[i].perp(); 
	  extras.push_back(HTfull);


	  ///////////////////////////////////////////////////////////////////////
	  // Dump leptons into "hadrons" for detector modeling (ID them later)
	  ///////////////////////////////////////////////////////////////////////
	  
	  hadrons.insert(hadrons.end(),leptons.begin(),leptons.end());


	  /////////////////////////////////////////////////////////////////////////////////////////
	  // Check if displaced particles are candidates for ATLAS displaced muon+tracks searches
	  // (1109.2242 [33/pb LHC7],  1210.7451 [5/fb LHC7],  ATLAS-CONF-2013-092 [20/fb LHC8]...currently using only the last one)
	  /////////////////////////////////////////////////////////////////////////////////////////

	  // I did not bother with the back-to-back muon veto (cosmic rejection)

	  // Because this analysis can depend on the global event configuration, first check for each displaced particle if it is charged.  If it is, ignore
	  // all tracks from its decay vertex, since we don't know if they could be successfully reconstructed.  (Technically only a possible issue for vertices
	  // within the tracker volume, but we conservatively apply to all charged displaced decays.)  Currently, this is the only analysis
	  // where such an explicit requirement is applied.  For other analyses, we apply any R-hadronization neutrality criteria as a weighting factor
	  // after reconstruction (e.g. in Zhen's Mathematica notebooks).
	  bool isCharged1 = false;  bool isCharged2 = false;
	  if (displacedSquarks) { isCharged1 = (uniformRandom() < chargedRhadronProb(1000006));  isCharged2 = (uniformRandom() < chargedRhadronProb(1000006)); }
	  if (displacedGluinos) { isCharged1 = (uniformRandom() < chargedRhadronProb(1000021));  isCharged2 = (uniformRandom() < chargedRhadronProb(1000021)); }
	  
	  // find displaced particles ("hadrons" = leptons+hadrons at this stage) from neutral displaced decays
	  vector<fastjet::PseudoJet> usable_displaced_hadrons;
	  for (int i = 0; i < hadrons.size(); i++) {
	    if (hadrons[i].user_info<ParticleInfo>().displacedCode() == 1 && !isCharged1 ||
		hadrons[i].user_info<ParticleInfo>().displacedCode() == 2 && !isCharged2)  usable_displaced_hadrons.push_back(hadrons[i]);
	  }
	  
	  double prob_muon = 0.70;  // extra muon ID efficiency factor (the matching track criteria are very tight, compared to a normal track)
	  double prob_vertex = 1.0;  // probability to successfully reconstruct the vertex (from Table 5 in the ATLAS-CONF, this is nearly 100%)
	  double prob_vertex_match = 0.70;  // probability to successfully match a vertex to a muon (this and the above are tuned to reproduce the last two rows of Table 5 in the ATLAS-CONF)
	  vector<fastjet::PseudoJet> ATLAStracks;
	  getATLAStracks(usable_displaced_hadrons,ATLAStracks);
	  vector<fastjet::PseudoJet> ATLASmuons;
	  vector<fastjet::PseudoJet> ATLASvertices;  // treated as 4-vectors (so that we can compute mass) with displaced coordinates
	  bool goodMuon = false;  // event contains displaced high-pT muon
	  bool triggerMuon = false;  // weaker criterion:  doesn't need to pass IP2D cut (not necessarily used...check below!)
	  bool backToBackMuons = false;
	  // accumulate tracks into common vertices and look for triggerable muon
	  for (int i = 0; i < ATLAStracks.size(); i++) {
	    fastjet::PseudoJet part = ATLAStracks[i];
	    bool thisGoodMuon = false;
	    bool thisTriggerMuon = false;
	    double ip2d = part.user_info<TrackInfo>().IP2D();
	    if (abs(part.user_index()) == 13 && part.pt() > 55 && fabs(part.rapidity()) < 1.07) {
	      thisTriggerMuon = true;
	      if (ip2d > 1.5e-3 && uniformRandom() < prob_muon) { thisGoodMuon = true; ATLASmuons.push_back(ATLAStracks[i]); }
	    }
	    goodMuon += thisGoodMuon;
	    triggerMuon += thisTriggerMuon;
	    fastjet::PseudoJet thisVert = part.user_info<TrackInfo>().v();
	    bool matchedToVertex = false;
	    for (int j = 0; j < ATLASvertices.size(); j++) {
	      if (pTot(thisVert-ATLASvertices[j].user_info<TrackInfo>().v()) < 1.e-6) {  // found matching vertex
		TrackInfo track_info = ATLASvertices[j].user_info<TrackInfo>();
		if (ip2d > 2.0e-3) {  // add it to the vertex's track collection if it's IP2D is big enough
		  ATLASvertices[j] += part;  // accumulate 4-vector associated to vertex
		  track_info.set_index(track_info.index()+1);  // here, TrackInfo "index" counts # of tracks in this vertex
		}
		ATLASvertices[j].set_user_info(new TrackInfo(track_info));
		matchedToVertex = true;
		if (thisGoodMuon)  ATLASvertices[j].set_user_index(13);  // fastjet user_index indicates if the vertex contains a displaced muon
		break;
	      }
	    }
	    if (!matchedToVertex) {  // couldn't find a matching vertex?  start a new one
	      fastjet::PseudoJet newVertex(0,0,0,0);
	      TrackInfo track_info = part.user_info<TrackInfo>();   track_info.set_index(0);  // initialize as containing 0 tracks
	      if (ip2d > 2.0e-3) {  // add it to the new vertex's track collection if it's IP2D is big enough
		newVertex += part;
		track_info.set_index(track_info.index()+1);
	      }
	      newVertex.set_user_info(new TrackInfo(track_info));
	      newVertex.set_user_index(0);
	      if (thisGoodMuon)  newVertex.set_user_index(13);  // note that may have a good muon, even if track is not accumulated due to small IP2D
	      ATLASvertices.push_back(newVertex);
	    }
	  }
	  // if a good muon is found, tally up the displaced vertices
	  bool goodVertex = false;
	  bool goodVertexLite = false;
	  if (goodMuon) {
	    for (int j = 0; j < ATLASvertices.size(); j++) {
	      double rad = ATLASvertices[j].user_info<TrackInfo>().v().pt();  double z = ATLASvertices[j].user_info<TrackInfo>().v().pz();
	      if (rad > 0.18 || rad < 0.004 || fabs(z) > 0.30)  continue;  // outside of usable volume  (r > 4mm is explicit ATLAS cut)
	      //cout << ATLASvertices[j].user_info<TrackInfo>().index()  << "  " << ATLASvertices[j].m() << endl;////
	      /////double prob_vertex = 0.5 * (1-rad/0.18) * (1-fabs(z)/0.30);  // simple linear vertex efficiency falloff model (see fig 3, ATLAS-CONF-2013-092)
	      if (uniformRandom() > prob_vertex)  continue;
	      if (ATLASvertices[j].user_info<TrackInfo>().index() < 5 || ATLASvertices[j].m() < 10)  continue;  // high-multiplicity / high-mass vertex
	      goodVertexLite = true;  // event contains a good vertex in an event with a displaced muon
	      if (ATLASvertices[j].user_index() == 13 && uniformRandom() < prob_vertex_match) {
		goodVertex = true;  // displaced muon associated to vertex
	      }
	    }
	  }

	  if (goodVertex)  nATLASmuonDV += displacedWeight;
	  if (goodVertexLite)  nATLASmuonDVlite += displacedWeight; 

	  
	  ////////////////////////////
	  // CMS-like detector model
	  ////////////////////////////
	  
	  vector<fastjet::PseudoJet> detector_particles;
	  detectorize(hadrons,detector_particles);
	  vector<fastjet::PseudoJet> detector_hadrons;  // for jet clustering...to be filled below after lepton ID

	  
	  /////////////////////////////////
	  // Lepton ID and isolation
	  /////////////////////////////////

	  // as in CMS PAS EXO-12-037, displaced dileptons
	  vector<fastjet::PseudoJet> detector_preLeptons;
	  vector<fastjet::PseudoJet> detector_electrons;
	  vector<fastjet::PseudoJet> detector_muons;
	  vector<fastjet::PseudoJet> detector_hadronTracks;
	  for (int i = 0; i < detector_particles.size(); i++) {
	    fastjet::PseudoJet part = detector_particles[i];
	    if (part.user_info<TrackInfo>().isTrack() && (abs(part.user_index())==11 || abs(part.user_index())==13) 
		&& part.perp()>ptMinLep && fabs(part.eta())<etaMaxTrack && uniformRandom()<IDeff(part.user_index(),part.perp())/promptTrackingEfficiency) 
	      detector_preLeptons.push_back(part);
	    else {
	      detector_hadrons.push_back(part);
	      if (part.user_info<TrackInfo>().isTrack())   detector_hadronTracks.push_back(part);
	    }
	  }
	  for (int i = 0; i < detector_preLeptons.size(); i++) {
	    fastjet::PseudoJet part = detector_preLeptons[i];
	    double Rmax = 0.3;
	    double Rmin = 0.0;///0.03;  // CMS uses a hollow isolation cone, but this region can be part of the lepton ID, so to be cautious we use a solid cone
	    int absid = abs(part.user_index());
	    if (absid==11)  Rmin = 0.0;////0.04;
	    double pTcone = 0;
	    double isoFrac = 0.1;
	    for (int j = 0; j < detector_hadronTracks.size(); j++) {
	      double dr = DR(part,detector_hadronTracks[j]);
	      if (dr > Rmin && dr < Rmax)  pTcone += detector_hadronTracks[j].perp();
	    }
	    if      (pTcone/part.perp() < isoFrac && absid==11)   detector_electrons.push_back(part);
	    else if (pTcone/part.perp() < isoFrac && absid==13)   detector_muons.push_back(part);
	    else                                                  detector_hadrons.push_back(part);  // non-isolated leptons thrown back into "hadrons" for later jet clustering (realistic??)
	  }


	  ////////////////////////////////////////////////////////
	  // Displaced dilepton analysis (CMS PAS EXO-12-037)
	  ////////////////////////////////////////////////////////
	  
	  bool passDisplacedDilepton = false;
	  for (int eOrMu = 0; eOrMu < 2; eOrMu++) {  // eOrMu=0 electron, eOrMu=1 muon
	    vector<fastjet::PseudoJet> detector_leptons;
	    if (eOrMu==0)  detector_leptons = detector_electrons;
	    else           detector_leptons = detector_muons;
	    for (int i = 0; i < detector_leptons.size(); i++)   for (int j = i+1; j < detector_leptons.size(); j++) {
	      fastjet::PseudoJet l1 = detector_leptons[i];
	      fastjet::PseudoJet l2 = detector_leptons[j];
	      if (abs(l1.eta()) > 2.0 || abs(l2.eta()) > 2.0)  continue;  // eta cuts
	      if (eOrMu==0 && (max(l1.perp(),l2.perp()) < 40 || min(l1.perp(),l2.perp()) < 25))  continue;  // electron pT cuts
	      if (eOrMu==1 && (l1.perp() < 26 || l2.perp() < 26))  continue;  // muon pT cuts
	      if ((l1+l2).m() < 15)  continue;  // J/psi & Upsilon veto
	      if ( (l1.px()*l2.px()+l1.py()*l2.py()+l1.pz()*l2.pz())/pTot(l1)/pTot(l2) < -0.79 )  continue;  // cosmic ray veto
	      if (eOrMu==0 && DR(l1,l2) < 0.1)  continue;  // electron DeltaR cut (each electron ECAL pattern has a "radius" ~ 0.1...helps H(1000)->2*X(20) to match CMS)
	      if (eOrMu==1 && DR(l1,l2) < 0.2)  continue;  // muon DeltaR cut
	      if (l1.user_info<TrackInfo>().IP2D() < 250e-6 || l2.user_info<TrackInfo>().IP2D() < 250e-6)  continue;  // demand "large" 2D impact parameters
	      if ( pTot(l1.user_info<TrackInfo>().v()-l2.user_info<TrackInfo>().v()) > 10e-6 )  continue;  // "same" vertex (to some arbitrary resolution)
	      fastjet::PseudoJet vertex = 0.5*(l1.user_info<TrackInfo>().v()+l2.user_info<TrackInfo>().v());  // average the two vertex positions
	      fastjet::PseudoJet p12 = l1+l2;
	      if ( (p12.px()*vertex.px()+p12.py()*vertex.py())/p12.perp()/vertex.perp() < 0 )  continue;  // |DeltaPhi| cut
	      passDisplacedDilepton = true;  // pass on an event-by-event basis if at least one good pair is found
	    }
	  }
	  if (passDisplacedDilepton)  nEventsPassDisplacedDilepton += displacedWeight;

	  
	  ////////////////////////////////////////////////////////
	  // Displaced e+mu analysis (CMS 1409.4789)
	  ////////////////////////////////////////////////////////
	  
	  // This can have somewhat different lepton isolation requirements than above, so just reprocess the detector-level particles

	  bool passDisplacedElectronMuonSR3 = false;
	  bool passDisplacedElectronMuonSR2 = false;
	  bool passDisplacedElectronMuonSR1 = false;
	  vector<fastjet::PseudoJet> detector_preLeptons_emu;
	  vector<fastjet::PseudoJet> detector_electrons_emu;
	  vector<fastjet::PseudoJet> detector_muons_emu;
	  vector<fastjet::PseudoJet> detector_hadrons_emu;  // this will not include non-isolated leptons
	  for (int i = 0; i < detector_particles.size(); i++) {
	    fastjet::PseudoJet part = detector_particles[i];
	    if (part.user_info<TrackInfo>().isTrack() && (abs(part.user_index())==11 || abs(part.user_index())==13) 
		&& part.perp()>25. && fabs(part.eta())<etaMaxTrack && uniformRandom()<IDeff(part.user_index(),part.perp())/promptTrackingEfficiency) 
	      detector_preLeptons_emu.push_back(part);
	    else   detector_hadrons_emu.push_back(part);
	  }
	  fastjet::JetDefinition jetDef_emu(fastjet::antikt_algorithm, 0.5, fastjet::E_scheme, fastjet::Best);
	  fastjet::ClusterSequence clusterSeq_emu(detector_hadrons_emu, jetDef_emu);
	  vector<fastjet::PseudoJet> jets_emu = clusterSeq_emu.inclusive_jets(10.);  // low pT threshold set by CMS for isolation from soft jets
	  for (int i = 0; i < detector_preLeptons_emu.size(); i++) {
	    fastjet::PseudoJet part = detector_preLeptons_emu[i];
	    if(!(part.user_info<TrackInfo>().IP2D() > 0.01e-2)) continue;  // must be displaced by more than 0.01 cm (100 microns)
	    int absid = abs(part.user_index());
	    double pTcone = 0;
	    double Rmax = 0.3;
	    double isoFrac = 0.1;
	    bool foundNearbyJet = false;
	    for (int j = 0; j < detector_hadrons_emu.size(); j++) {
	      double dr = DR(part,detector_hadrons_emu[j]);
	      if (dr < Rmax)  pTcone += detector_hadrons_emu[j].perp();
	    }
	    for (int j = 0; j < jets_emu.size(); j++) {
	      double dr = DR(part,jets_emu[j]);
	      if (dr < 0.5)  foundNearbyJet = true;
	    }
	    if      (pTcone/part.perp() < isoFrac && !foundNearbyJet && absid==11)   detector_electrons_emu.push_back(part);
	    else if (pTcone/part.perp() < isoFrac && !foundNearbyJet && absid==13)   detector_muons_emu.push_back(part);
	    // non-isolated leptons are simply lost...hopefully not a big deal, and close to what is actually done
	  }
	  if (detector_electrons_emu.size() == 1 && detector_muons_emu.size() == 1 && detector_electrons_emu[0].user_index()*detector_muons_emu[0].user_index() < 0 && 
	      DR(detector_electrons_emu[0],detector_muons_emu[0]) > 0.5) {  // exactly 1e1mu, OS, well-separated
	    double de  = detector_electrons_emu[0].user_info<TrackInfo>().IP2D();
	    double dmu = detector_muons_emu[0]    .user_info<TrackInfo>().IP2D();
	    if (de < 2e-2 && dmu < 2e-2) {  // can't be *too* displaced!  work in region where tracking efficiency hasn't fallen off yet
	      if      (de > 0.10e-2 && dmu > 0.10e-2)  passDisplacedElectronMuonSR3 = true;
	      else if (de > 0.05e-2 && dmu > 0.05e-2)  passDisplacedElectronMuonSR2 = true;
	      else if (de > 0.02e-2 && dmu > 0.02e-2)  passDisplacedElectronMuonSR1 = true;
	    }
	  }

	  if (uniformRandom() < 0.80) {   // efficiency fudge factor to better match CMS
	    if      (passDisplacedElectronMuonSR3)  nEventsPassDisplacedElectronMuonSR3 += displacedWeight;
	    else if (passDisplacedElectronMuonSR2)  nEventsPassDisplacedElectronMuonSR2 += displacedWeight;
	    else if (passDisplacedElectronMuonSR1)  nEventsPassDisplacedElectronMuonSR1 += displacedWeight;
	  }


	  //////////////////////
	  // Jet clustering
	  //////////////////////

	  //detector_hadrons.insert(detector_hadrons.end(),detector_electrons.begin(),detector_electrons.end());  // assume that isolated electrons can be displaced jet constituents, since normal electron ID would likely fail (though I don't add back in isolated muons...no calorimeter pattern)

	  fastjet::JetDefinition jetDef(fastjet::antikt_algorithm, Rjet, fastjet::E_scheme, fastjet::Best);
	  fastjet::ClusterSequence clusterSeq(detector_hadrons, jetDef);
	  vector<fastjet::PseudoJet> inclusiveJets = clusterSeq.inclusive_jets(ptMinJet);
	  double jetHT = 0;
	  for (int i = 0; i < inclusiveJets.size(); i++) {
	    jetHT += inclusiveJets[i].pt();
	    if (inclusiveJets[i].pt() > ptMinRecoJet && fabs(inclusiveJets[i].eta()) < etaMaxJet)   jets.push_back(inclusiveJets[i]);
	  }
	  jets = sorted_by_pt(jets);
	  bcTag(jets,b_hadrons,Rjet);


	  /////////////////////////////////////////////////////////////
	  //  Apply the displaced dijet trigger of CMS PAS EXO-12-038
	  //  (Also do some setup for vertex-finding)
	  /////////////////////////////////////////////////////////////

	  if (jetHT < HTmin || jets.size() < 2)    { nZeroPassHigh++;  continue; }

	  int nJetPass=0;
	  vector< vector<fastjet::PseudoJet> > promptTracks,displacedTracks;
	  vector<double> promptEfrac;
	  for(int i = 0; i < jets.size(); i++) {
	    vector<fastjet::PseudoJet> constituents_of_jet = clusterSeq.constituents(jets[i]);
	    int nTrackLowIP3D = 0;
	    double promptEfrac_thisJet = 0;
	    vector<fastjet::PseudoJet> promptTracks_thisJet,displacedTracks_thisJet;
	    for(int j = 0; j < constituents_of_jet.size(); j++) {
	      if(!constituents_of_jet[j].user_info<TrackInfo>().isTrack())  continue;
	      TrackInfo track_info = constituents_of_jet[j].user_info<TrackInfo>();   track_info.set_index(i);
	      constituents_of_jet[j].set_user_info(new TrackInfo(track_info));  // remember which jet this track lives in
	      if(constituents_of_jet[j].user_info<TrackInfo>().IP3D() < 0.3e-3) nTrackLowIP3D++;
	      if(constituents_of_jet[j].user_info<TrackInfo>().IP2D() < 0.5e-3) { promptEfrac_thisJet += constituents_of_jet[j].E()/jets[i].E();  promptTracks_thisJet.push_back(constituents_of_jet[j]); }
	      else  displacedTracks_thisJet.push_back(constituents_of_jet[j]);
	    }
	    promptTracks.push_back(promptTracks_thisJet);  displacedTracks.push_back(displacedTracks_thisJet);  promptEfrac.push_back(promptEfrac_thisJet);
	    if (nTrackLowIP3D < 2 && promptEfrac_thisJet < 0.15)  nJetPass++;
	  }


	  // turn this off if you want just the basic HT trigger in operation, for building discriminating variable plots a la CMS
	  // (also, the novel bits of the trigger are totally redundant with downstream cuts anyway, so feel free to leave it off in general)
	  //if (nJetPass < 2)  continue;


	  //////////////////////////////////////////////////////////////////////////////////
	  //  Look for displaced dijet pairs, using algorithms based on CMS PAS EXO-12-038
	  //////////////////////////////////////////////////////////////////////////////////

	  // NOTE: It is not clear from CMS's paper whether we're allowed to use a jet in multiple valid pairs.  Here we conservatively set aside
	  //       a jet once it's been used.  We iterate through jets descending in pT.  You should consider switching this method on/off to see its effect.
	  //       We cannot model CMS's vertex-finding procedure in detail.  Here we simply take the vertex within a jet-pair that has the highest
	  //       number of tracks.  (Assuming this would be the most likely one to be found!)

	  
	  vector<int> used_up_jet_indices;
	  double vertex_resolution = 100e-6;  // maximum 2D distance between vertices (otherwise merged)
	  for (int i = 0; i < jets.size(); i++) {
	    for (int j = i+1; j < jets.size(); j++) {
	      bool used_up = false;
	      for (int k = 0; k < used_up_jet_indices.size(); k++)  if (i==used_up_jet_indices[k] || j==used_up_jet_indices[k])  used_up=true;
	      if (used_up) continue;
	      fastjet::PseudoJet jetPair = jets[i]+jets[j];
	      vector<fastjet::PseudoJet> vertices;  vector< vector<fastjet::PseudoJet> > vertex_tracks; 
	      vector<fastjet::PseudoJet> allDisplacedTracks;
	      allDisplacedTracks.insert(allDisplacedTracks.end(),displacedTracks[i].begin(),displacedTracks[i].end());
	      allDisplacedTracks.insert(allDisplacedTracks.end(),displacedTracks[j].begin(),displacedTracks[j].end());
	      for (int k = 0; k < allDisplacedTracks.size(); k++) { 
		fastjet::PseudoJet this_vertex = allDisplacedTracks[k].user_info<TrackInfo>().v();
		bool found_associated_vertex = false;
		for (int l = 0; l < vertices.size(); l++) {
		  if ((this_vertex-vertices[l]).perp() < vertex_resolution) { vertex_tracks[l].push_back(allDisplacedTracks[k]); found_associated_vertex=true; break; }
		}
		if (!found_associated_vertex) {
		  vertices.push_back(this_vertex);
		  vector<fastjet::PseudoJet> this_vertex_tracks;  this_vertex_tracks.push_back(allDisplacedTracks[k]);  vertex_tracks.push_back(this_vertex_tracks);
		}
	      }
	      int candidate_vertex_index = -1;
	      int nTracksMax = 0;
	      for (int k = 0; k < vertices.size(); k++)  if (vertex_tracks[k].size() > nTracksMax) { candidate_vertex_index=k; nTracksMax=vertex_tracks[k].size(); }
	      if (candidate_vertex_index < 0)  continue;
	      double Lxy = vertices[candidate_vertex_index].perp();
	      if (cutoff_bc && (abs(jets[i].user_index())==5 || abs(jets[j].user_index())==4) && Lxy < 0.01)  continue;  // ad hoc 1 cm cutoff on Lxy, to regulate "doubly-displaced" decays with b/c-quarks, which might be resolved 
	      if (cutoffLxy && Lxy < 0.01)  continue;  // or apply the cutoff regardless of flavor (e.g. to simulate b/c content when none was simulated) 
	      bool foundTrack_jet1 = false;
	      bool foundTrack_jet2 = false;
	      fastjet::PseudoJet vertex_4vector = blankJet;
	      int nPositiveIP2D = 0;
	      double pjUnit_x = jetPair.px()/jetPair.pt();  double pjUnit_y = jetPair.py()/jetPair.pt();
	      vector<double> LxyTracks;  // transverse crossing points along on the dijet trajectory
	      for (int k = 0; k < vertex_tracks[candidate_vertex_index].size(); k++) {
		vertex_4vector += vertex_tracks[candidate_vertex_index][k];
		if (vertex_tracks[candidate_vertex_index][k].user_info<TrackInfo>().index() == i) foundTrack_jet1 = true;
		if (vertex_tracks[candidate_vertex_index][k].user_info<TrackInfo>().index() == j) foundTrack_jet2 = true;
		fastjet::PseudoJet IP2Dpos = vertex_tracks[candidate_vertex_index][k].user_info<TrackInfo>().IP2Dpos();
		if (IP2Dpos.px()*jetPair.px()+IP2Dpos.py()*jetPair.py() > 0)  nPositiveIP2D++;
		fastjet::PseudoJet center_of_curvature = vertex_tracks[candidate_vertex_index][k].user_info<TrackInfo>().C();
		double Cx = center_of_curvature.px()*pjUnit_x + center_of_curvature.py()*pjUnit_y;
		double Cy = sqrt(center_of_curvature.perp()*center_of_curvature.perp()-Cx*Cx);
		double radius_of_curvature = vertex_tracks[candidate_vertex_index][k].user_info<TrackInfo>().R();
		if (radius_of_curvature > Cy) {
		  double Lxy1 = Cx + sqrt(radius_of_curvature*radius_of_curvature-Cy*Cy);
		  double Lxy2 = Cx - sqrt(radius_of_curvature*radius_of_curvature-Cy*Cy);
		  if      (Lxy1 > 0 && Lxy2 < 0)  LxyTracks.push_back(Lxy1);
		  else if (Lxy2 > 0 && Lxy1 < 0)  LxyTracks.push_back(Lxy2);
		  else if (Lxy1 > 0 && Lxy2 > 0) {
		    if (fabs(Lxy1-Lxy) < fabs(Lxy2-Lxy))  LxyTracks.push_back(Lxy1);
		    else                                  LxyTracks.push_back(Lxy2);
		  }
		}
	      }
	      if (!foundTrack_jet1 || !foundTrack_jet2)  continue;
	      if (vertex_4vector.m() < 4 || vertex_4vector.pt() < 8)  continue;
	      // *** finished pre-selection cuts (Section 4 of EXO-12-038) ***

	      // implement vertex-finding efficiency correction (a "fudge factor", assuming vertexing fails near the edge of the tracker)
	      /////double prob_vertex = 1;///
	      //////double prob_vertex = exp(-Lxy/0.3);///
	      ////double prob_vertex = max(0.,1-Lxy/(0.6));///
	      double prob_vertex = max(0.,(1-Lxy/(0.6))*(1-Lxy/(0.6)));
	      if (uniformRandom() > prob_vertex)  continue;

	      // if we made it this far, this jet-pair is officially a "candidate".  conservatively, do not allow these jets to be re-used. 
	      used_up_jet_indices.push_back(i);  used_up_jet_indices.push_back(j);

	      ////  UNCOMMENT TO "TURN OFF" FULL SELECTION
	      ///nPairsPass++;   continue;//////////////////
	      ////

	      double positiveIPfraction = nPositiveIP2D*1./vertex_tracks[candidate_vertex_index].size();
	      sort(LxyTracks.begin(),LxyTracks.end());

	      vector<double> LxyTrackCollection;   double LxyTrackAvg = -1;  double LxyTrackRMS = 1e10; 
	      for (int k = 0; k < LxyTracks.size(); k++) {  // consider all collections within a sliding window (considering in turn each crossing point as a low edge)
		vector<double> this_LxyTrackCollection;
		double this_LxyTrackAvg = 0;
		double this_LxyTrackRMS = 0;
		for (int l = k; l < LxyTracks.size(); l++) {
		  if (LxyTracks[l]-LxyTracks[k] < 0.15*Lxy) {
		    this_LxyTrackCollection.push_back(LxyTracks[l]);  
		    this_LxyTrackAvg += LxyTracks[l];
		    //this_LxyTrackRMS += (LxyTracks[l]-Lxy)*(LxyTracks[l]-Lxy);
		  }
		}
		this_LxyTrackAvg /= this_LxyTrackCollection.size();
		for (int l = 0; l < this_LxyTrackCollection.size(); l++)   this_LxyTrackRMS += (LxyTracks[l]-this_LxyTrackAvg)*(LxyTracks[l]-this_LxyTrackAvg)/(this_LxyTrackCollection.size()-1);
		this_LxyTrackRMS = sqrt(this_LxyTrackRMS)/Lxy;
		if ( this_LxyTrackCollection.size()  > LxyTrackCollection.size() ||
		    (this_LxyTrackCollection.size() == LxyTrackCollection.size() && this_LxyTrackRMS < LxyTrackRMS) ) {
		  LxyTrackCollection = this_LxyTrackCollection;
		  LxyTrackRMS = this_LxyTrackRMS;
		}
	      }
	      int nTracksLxy = LxyTrackCollection.size();
	      if (nTracksLxy < 2)  continue;  // just guessing that this is implicitly one of the cuts...their p.d.f. for this variable starts at 2 tracks, and the RMS is undefined for one measurement
	      
	      // CMS likelihood-ratio discriminator
	      if (nTracksMax > maxVertexTrackMultiplicity)  nTracksMax = (int)maxVertexTrackMultiplicity;
	      int vertexTrackMultiplicityBin = nBinsVertexTrackMultiplicity*1.*(nTracksMax-minVertexTrackMultiplicity)/(maxVertexTrackMultiplicity-minVertexTrackMultiplicity);
	      int positiveIPfractionBin = nBinsPositiveIPfraction*1.*(positiveIPfraction-minPositiveIPfraction)/(maxPositiveIPfraction-minPositiveIPfraction);
	      if (nTracksLxy > maxClusterTrackMultiplicity)  nTracksLxy = (int)maxClusterTrackMultiplicity;
	      int clusterTrackMultiplicityBin = nBinsClusterTrackMultiplicity*1.*(nTracksLxy-minClusterTrackMultiplicity)/(maxClusterTrackMultiplicity-minClusterTrackMultiplicity);
	      if (LxyTrackRMS > maxClusterRMS)  LxyTrackRMS = maxClusterRMS*0.999;
	      int clusterRMSBin = nBinsClusterRMS*1.*(LxyTrackRMS-minClusterRMS)/(maxClusterRMS-minClusterRMS);
	      double probS = 1;
	      probS *= vertexTrackMultiplicityPDF_S[vertexTrackMultiplicityBin];
	      probS *= positiveIPfractionPDF_S[positiveIPfractionBin];
	      probS *= clusterTrackMultiplicityPDF_S[clusterTrackMultiplicityBin];
	      probS *= clusterRMSPDF_S[clusterRMSBin];
	      double probB = 1;
	      probB *= vertexTrackMultiplicityPDF_B[vertexTrackMultiplicityBin];
	      probB *= positiveIPfractionPDF_B[positiveIPfractionBin];
	      probB *= clusterTrackMultiplicityPDF_B[clusterTrackMultiplicityBin];
	      probB *= clusterRMSPDF_B[clusterRMSBin];
	      double LR = 1;  if (probS+probB > 0)    LR = probS / (probS+probB);
	      //cout << probS << "  " << probB << "  " << LR << endl;
	     

	      // if desired, write information about this displaced dijet candidate to file for study in ROOT (read by CMSplots.C)
	      if (writeVertexFile)  vertexFile << nTracksMax << " " << positiveIPfraction << " " << nTracksLxy << " " 
					       << LxyTrackRMS << " " << LR << " " << Lxy << " " 
					       << promptTracks[i].size() << " "<< promptTracks[j].size() << " " 
					       << promptEfrac[i] << " " << promptEfrac[j] 
					       << " " << jetPair.pt() << endl;
	      
	      //LR = 1;///////////
	      // implement final selection criteria (Section 5, Table 1)
	      if (promptTracks[i].size() > 1 || promptTracks[j].size() > 1)  continue;  // at most 1 prompt track per jet
	      if (promptEfrac[i] < 0.15 && promptEfrac[j] < 0.15 && LR > 0.9) {    // pass Low-Lxy selection
		nPairsPassLow += displacedWeight; 
		sumW2Low += displacedWeight*displacedWeight;
		//cout << "PASS:  weight = " << displacedWeight << endl;
	      }
	      if (promptEfrac[i] < 0.09 && promptEfrac[j] < 0.09 && LR > 0.8) {   // pass High-Lxy selection
		nPairsPassHigh += displacedWeight;
		nPairsPassHighThisEvent++;
		sumW2High += displacedWeight*displacedWeight;
	      }
	    }
	  }


	  ///////////////////////////////
	  // Write event to output file
	  ///////////////////////////////	  

	  //writeOutput(jetFile,fileIt,eventNum,weight,godParticles,b_hadrons,isoLeptons,jets,extras);

	  
	  nEventsFinal++;

	  if (nPairsPassHighThisEvent==0) nZeroPassHigh++;
	  if (nPairsPassHighThisEvent==1) nOnePassHigh++;
	  if (nPairsPassHighThisEvent==2) nTwoPassHigh++;
	  
	  
	} // end this event, go back and read next event
	
	datfile.close();

      } // end iteration over data files for a given simulation
      
     

      if (nFiles < 1) {
	cout << "FAILED TO LOAD FIRST FILE" << endl;
	continue; 
      }
      
      
      // output file of total cross section, # generated, # passed through filter
      sigmaAvg *= nEventsLoaded*1./nEventsGenerated;  // in case there was a generator-level veto above, rescale cross section
      ofstream sigmaFile;
      filename = outputDir+"/sigma."+fileKey+"."+usertag[tr]+".dat";
      if (usertag[tr] == "") filename = outputDir+"/sigma."+fileKey+".dat";
      ///sigmaFile.open(filename.c_str());
      ///sigmaFile << sigmaAvg << " " << nEventsLoaded << " " << nEventsFinal << endl;
      //
      //cout << endl << "-------------------------------------------------------" << endl;
      //cout << nEventsGenerated << " events generated" << endl;
      //cout << nEventsLoaded    << " loaded"           << endl;
      //cout << "---------------------------------------------------------------" << endl;
      //cout << nEventsFinal << " (" << nEventsFinal*1./nEventsLoaded << ") pass reconstruction criteria" << endl;
      //cout << nPairsPass << " (" << nPairsPass*1./nEventsLoaded/2. << ") vertices pass reconstruction criteria" << endl;
      //cout << "---------------------------------------------------------------" << endl;
      //cout << nEventsSubstructure << " successful decluster (" << nEventsSubstructure*1./nEventsFinal << ")" << endl << endl;


      
      //cout << endl;
      //cout << "---------------------------------------" << endl;
      //cout << " total failures           :  " << nEventsLoaded-nEventsFinal << endl;
      //cout << endl;
      

      cout << endl;
      //cout << "Acceptance            = " << nAccepted << "/" << 2*nEventsLoaded << " = " << nAccepted/2./nEventsLoaded*100 << "%"<< endl; 
      cout << "Low-Lxy selection       = " << nPairsPassLow << "/" << 2*nEventsLoaded << " = " << nPairsPassLow/2./nEventsLoaded*100 << "%";
      cout << "     (fractional error " << sqrt(sumW2Low)/nPairsPassLow << ")" << endl;
      cout << "High-Lxy selection      = " << nPairsPassHigh << "/" << 2*nEventsLoaded << " = " << nPairsPassHigh/2./nEventsLoaded*100 << "%";
      cout << "     (fractional error " << sqrt(sumW2High)/nPairsPassHigh << ")" << endl;

      //cout << "Efficiency            = " << nPairsPass*1./nAccepted*100 << "%" << endl;      
      //cout << endl;

      cout << "ATLAS muon efficiency   = " << " (" << nMCeventsATLASmuon << " MC events) = " << nEventsATLASmuon*100./nEventsLoaded << "%" << endl;

      //cout << "naive    CHAMP fraction:  " << nCHAMPoutsideDetector*100./nCHAMPfiducial << "%  (" << nCHAMPoutsideDetector << "/" << nCHAMPfiducial << ")" << endl;
      cout << "nominal  CHAMP fraction = " << nCHAMPgood << "/" << nCHAMPfiducial << " = " << nCHAMPgood*100./nCHAMPfiducial << "%" << endl;
      cout << "high-MET CHAMP fraction = " << nCHAMPgoodMET << "/" << nCHAMPfiducialMET << " = " << nCHAMPgoodMET*100./nCHAMPfiducialMET << "%" << endl;
      cout << endl;

      cout << "CMS displaced dilepton = " << nEventsPassDisplacedDilepton << "/" << nEventsLoaded << " = " << nEventsPassDisplacedDilepton*1./nEventsLoaded << endl;
      cout << endl;

      cout << "CMS displaced e+mu SR1 = " << nEventsPassDisplacedElectronMuonSR1 << "/" << nEventsLoaded << " = " << nEventsPassDisplacedElectronMuonSR1*1./nEventsLoaded << endl;
      cout << "CMS displaced e+mu SR2 = " << nEventsPassDisplacedElectronMuonSR2 << "/" << nEventsLoaded << " = " << nEventsPassDisplacedElectronMuonSR2*1./nEventsLoaded << endl;
      cout << "CMS displaced e+mu SR3 = " << nEventsPassDisplacedElectronMuonSR3 << "/" << nEventsLoaded << " = " << nEventsPassDisplacedElectronMuonSR3*1./nEventsLoaded << endl;
      cout << endl;

      cout << "ATLAS mu+tracks muon-vertices = " << nATLASmuonDV << "/" << nEventsLoaded << " = " << nATLASmuonDV*1./nEventsLoaded*100 << "%";
      cout << "     (fractional error " << 1/sqrt(1.*nATLASmuonDV) << ")" << endl;
      cout << "ATLAS mu+tracks any vertices = " << nATLASmuonDVlite << "/" << nEventsLoaded << " = " << nATLASmuonDVlite*1./nEventsLoaded*100 << "%";
      cout << "     (fractional error " << 1/sqrt(1.*nATLASmuonDVlite) << ")" << endl;
      // cout << "ATLAS mu+tracks muon-vertices r=[5mm,35mm] = " << nATLASmuonDV_r5mm_r25mm << "/" << nVertices_r5mm_r25mm << " = " << 1.*nATLASmuonDV_r5mm_r25mm/nVertices_r5mm_r25mm*100 << "%";
      //cout << "     (fractional error " << 1/sqrt(1.*nATLASmuonDV_r5mm_r25mm) << ")" << endl;
      //cout << "ATLAS mu+tracks muon-vertices r=[130mm,180mm] = " << nATLASmuonDV_r130mm_r180mm << "/" << nVertices_r130mm_r180mm << " = " << 1.*nATLASmuonDV_r130mm_r180mm/nVertices_r130mm_r180mm*100 << "%";
      //cout << "     (fractional error " << 1/sqrt(1.*nATLASmuonDV_r130mm_r180mm) << ")" << endl;

      cout << endl;

      cout << "ATLAS HCAL-jets       = :  " <<  " (" << nMCeventsATLASHCAL << " MC events) = " << nEventsATLASHCAL << "/" << nEventsLoaded << " = " <<  nEventsATLASHCAL/nEventsLoaded*100 << "%" << endl;
      cout << "ATLAS HCAL-jets loose = :  " <<  " (" << nMCeventsATLASHCAL_loose << " MC events) = " << nEventsATLASHCAL_loose << "/" << nEventsLoaded << " = " <<  nEventsATLASHCAL_loose/nEventsLoaded*100 << "%" << endl;
      cout << endl;


      /*
      cout << "CMS displaced dijet breakdown" << endl;
      cout << "# events 0 vertices pass:  " << nZeroPassHigh << endl;
      cout << "# events 1 vertices pass:  " << nOnePassHigh << endl;
      cout << "# events 2 vertices pass:  " << nTwoPassHigh << endl;
      cout << endl;
      */

      if (LxyHisto) {
	cout << endl;
	cout << "Bin counts for truth Lxy" << endl;
	for (int i = 0; i < LxyHistoNbins; i++)  cout << "HISTONAMEHERE->SetBinContent(" << i+1 << ", " << LxyHistoEntries[i] << ");" << endl;
	cout << "---------------" << endl << "<Lxy> = " << LxyAvg/2./nEventsLoaded << endl;
      }
      

      jetFile.close();
      vertexFile.close();
      

      effFile << fileKey << " "
	      << lifetime*speed_of_light << "   " 
	      << nPairsPassLow/2./nEventsLoaded << " " << nPairsPassHigh/2./nEventsLoaded << "   " 
	      << nEventsATLASmuon*1./nEventsLoaded << "   " 
	      << nCHAMPgood*1./nCHAMPfiducial << " " << nCHAMPgoodMET*1./nCHAMPfiducialMET << "   " 
	      << nEventsPassDisplacedDilepton*1./nEventsLoaded << "   " 
	      << nATLASmuonDV*1./nEventsLoaded << " " << nATLASmuonDVlite*1./nEventsLoaded << "   "   // these numbers now include R-hadronization effects (charged are assumed unreconstructable)
	      << nEventsPassDisplacedElectronMuonSR1*1./nEventsLoaded << " " << nEventsPassDisplacedElectronMuonSR2*1./nEventsLoaded << " " << nEventsPassDisplacedElectronMuonSR3*1./nEventsLoaded << "   " 
	      << nEventsATLASHCAL*1./nEventsLoaded << " " << nEventsATLASHCAL_loose*1./nEventsLoaded 
	      << endl;

    } // end iteration over sims

  } // end iteration over "trees" (physics processes)  

}







///////////////////////////////////////////////////////////////////////////////////
// ADDITIONAL FUNCTION DEFINITIONS


//----------------------------------------------------------
// write dijet output file for ROOT analysis
//
void writeOutput(ofstream & jetFile, int iFile, int iEvent, float weight, const vector<fastjet::PseudoJet> & godParts,
                 const vector<fastjet::PseudoJet> & b_hads, const vector<fastjet::PseudoJet> & leptons,
		 const vector<fastjet::PseudoJet> & jets, const vector<double> & extras)
{
  jetFile << fixed << setprecision(2);

  if (weight==0)
    jetFile << iFile << " " << iEvent << " " << godParts.size() << endl;
  else
    jetFile << iFile << " " << iEvent << " " << godParts.size() << "  " << weight << endl;
  for (int it = 0; it < godParts.size(); it++) {                        // God particles
    jetFile << godParts[it].user_index() << "  " 
	    << godParts[it].px()         << " "
	    << godParts[it].py()         << " "
	    << godParts[it].pz()         << " "
	    << godParts[it].e()          << endl;
  }
  jetFile << "0 " << b_hads.size() << endl;
  for (int it = 0; it < b_hads.size(); it++) {            // b hads
    jetFile << b_hads[it].user_index() << "  " 
	    << b_hads[it].px()         << " "
	    << b_hads[it].py()         << " "
	    << b_hads[it].pz()         << " "
	    << b_hads[it].e()          << endl;
  }
  jetFile << "0   ";     // general event info, begin reconstruction-level quantities and objects
  for (int xIt = 0; xIt < extras.size(); xIt++) {
    jetFile << extras[xIt] << " ";
  }
  jetFile << endl;                                
  jetFile << "0 " << leptons.size() << endl;              // leptons
  for (int lepIt = 0; lepIt < leptons.size(); lepIt++) {
    jetFile << leptons[lepIt].user_index() << "  "
	    << leptons[lepIt].px() << " "
	    << leptons[lepIt].py() << " "
	    << leptons[lepIt].pz() << " "
	    << leptons[lepIt].e()  << " "
	    << "lepton" << endl;
  }
  jetFile << "0 " << jets.size() << endl;   // jets
  for (int jetIt = 0; jetIt < jets.size(); jetIt++) {
    jetFile << jets[jetIt].user_index() << "  "
	    << jets[jetIt].px() << " "
	    << jets[jetIt].py() << " "
	    << jets[jetIt].pz() << " "
	    << jets[jetIt].e()  << " "
	    << "jet" << endl;
  }

  jetFile << endl;
}


