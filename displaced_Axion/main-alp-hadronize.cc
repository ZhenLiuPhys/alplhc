//  Hadronize parton-level events from an LHE file, or generate directly in Pythia.  It switches to specific internal
//  processes when fed certain keys at the command-line (see below).

//  This version appends a code to each final-state particle, to indicate which primary particle they come from (if any).
//  You can also include an HT-trigger to save time.


#include "Pythia8/Pythia.h"
//#include "/home/novar/Program/pythia8186/include/Pythia8/Pythia.h"

using namespace Pythia8; 
//#include <string.h>
int idLongLived;   // if 0, looks for first particle of 2->2 pair in event record, excepting intermediate resonances
                        // if negative, effectively deactivated
                        // if you specify one of the internal generation processes at the command line, this gets reset
bool coloredLongLived; 
///////////// LLP related defs //////////////

int passedEvents = 0;

Vec4 DisplacedVertex; 

double HTcut = 100.0;

double lifetime = 1e-11; //in sec

double properTimeMin = 0;
double properTimeMax = lifetime*1e6;

const double speed_of_light = 3.e8;  // m/s

const double B = 3.8; //B field CMS tracker in tesla

double uniformRandom()
{
  return rand()*1./RAND_MAX;
}

double RestrictedExponential(double t1, double t2)
{
  double x1 = exp(-t1);
  double x2 = exp(-t2);
  return -log(x1 + (x2-x1)*uniformRandom() );
}

//location of 6 pT modules in m
double r1 = 0.200; double r2 = 0.350; double r3 = 0.500; double r4 = 0.700; double r5 = 0.850; double r6 = 1.100; 

/////////////////////////////////////////////



// shower-level trigger parameters
const double HTmin = 0;  // don't set higher than than the final jet-level HT trigger
const double showerPtInterrupt = 30;  // pT scale at which to interrupt the shower and check HT (should be comparable to minimum jet pT)

// SPECIAL:  veto emissions above a given pT scale, for poor-man's matching studies
const double pTshowerMax = -30;

// cap # of events to hadronize, if desired
const int nHadronizeMax = 1e9;

// global pointer to the pythia object
Pythia * pythiaPtr;

// list of R-hadron id codes
// R-hadrons must be treated specially, since they do not have a unique id
bool Rhadrons = false;
const int nRhadrons = 31;
int RhadronIDs[nRhadrons] = {1000993,1009113,1009213,1009223,1009313,1009323,1009333,1091114,1092114,1092214,1092224,1093114,1092314,1093224,1093314,1093324,1093334,1000612,1000622,1000632,1000642,1000652,1006113,1006211,1006213,1006223,1006311,1006313,1006321,1006323,1006333};
bool isRhadron(int id)
{
  for (int i = 0; i < nRhadrons; i++)  if (abs(id) == RhadronIDs[i])  return true;
  return false;
}


// determine heaviest flavor inside of a hadron
int getFlavor(int id) {
  int absid = abs(id);
  int digit2 = (absid - 1000*(absid/1000) - absid%100)/100;
  int digit3 = (absid - 10000*(absid/10000) - absid%1000)/1000;
  return max(digit2,digit3);
}

// "UserHooks" class for vetoing events at shower stage, before hadronization.
// Currently, veto on HT, after a few showering steps.
//

class MyUserHooks : public UserHooks {

public:

  MyUserHooks() {}
  ~MyUserHooks() {}
  
  virtual bool canVetoPT() {return true;}    // Allow a veto for the interleaved evolution in pT.
  virtual double scaleVetoPT() {return showerPtInterrupt;}    // Do the veto test at a specified pT scale.

  // Access the event in the interleaved evolution.
  virtual bool doVetoPT(int iPos, const Event& event) {

    // iPos <= 3 for interleaved evolution; skip others.
    if (iPos > 3) return false;

    // Extract a copy of the partons in the hardest system.
    subEvent(event);
    double HTj = 0;
    for (int i = 0; i < event.size(); ++i) { 
      Particle part = event[i];
      if (part.isFinal())  HTj += part.pT();
    }
    
    // Run the HT trigger
    if (HTj < HTmin) return true;     // Veto events which do not have enough "jet"-HT.

    return false;    // Do not veto events that got this far.
  }


private:

};
///


// find the closest parton to a given final-state particle
int getClosestParton(int ip, const Pythia & pythia, const vector<int> & finalPartonIndices)
{
  double DRmin = 1e10;
  double DRmaxAllowed = 0.5;  // max matching distance (controls high-mass outliers for final reconstructed particles)
  int closestParton = 0;
  for (unsigned int i = 0; i < finalPartonIndices.size(); i++) {
    double DR = REtaPhi(pythia.event[ip].p(),pythia.event[finalPartonIndices[i]].p());
    //cout << "      " << i << " (" << pythia.event[finalPartonIndices[i]].name() << "):  DR = " << DR << endl; 
    if (DR < DRmin && DR < DRmaxAllowed) { DRmin=DR; closestParton=finalPartonIndices[i]; }
  }
  return closestParton;
}




// Trace ancestry of final-state particles, in search of long-lived ancestors.
 
int getLongLivedAncestry(int ip, const Pythia & pythia, const vector<int> & longLivedIndices, const vector<int> & finalPartonIndices)
{
  //cout << endl << "start ancestry, id = " << pythia.event[ip].id() << endl;
  int iAncestor = ip;
  int nTries = 0;
  while (1) {
    int mo1 = pythia.event[iAncestor].mother1();
    int mo2 = pythia.event[iAncestor].mother2();
    if (mo1==0)  break;
    if (coloredLongLived && pythia.event[iAncestor].isHadron() && pythia.event[mo1].isParton() && pythia.event[mo2].isParton()) 
      iAncestor = getClosestParton(iAncestor,pythia,finalPartonIndices);
    else
      iAncestor = mo1;
    //cout << "  ancestor (" << iAncestor << "), id = " << pythia.event[iAncestor].id() << endl;
    for (unsigned int i = 0; i < longLivedIndices.size(); i++)  if (iAncestor==longLivedIndices[i])  return longLivedIndices[i];
    nTries++;  if(nTries > 1000)  break;  // avoid (rare) infinite loop
  }

  return 0;
}



//------------------------------------------------------------------------
//
int main(int argc, char ** argv) {

  // LHE file name
  string dir = ".";
  string lheFileName = "unweighted_events";
  string mass = "100";
  int nGen = 2000;//10000; //Number of events to be generated
  string userTag = "";
  if (argc >= 2)    dir = argv[1];
  if (argc >= 3)    lheFileName = argv[2];
  if (argc >= 4)    mass = argv[3];
  if (argc >= 5)    nGen = atoi(argv[4]);
  if (argc >= 6)    userTag = argv[5];
  if (userTag != "")  userTag = "."+userTag;

  // Generator
  //Pythia pythia(xmldir);
  Pythia pythia;
  pythiaPtr = &pythia;
  pythia.readString("PartonLevel:ISR = on"); 
  pythia.readString("PartonLevel:FSR = on"); 
  pythia.readString("PartonLevel:MPI = on"); 
  pythia.readString("HadronLevel:Hadronize = on"); 

  // Prevent "power shower" in BSM pair production events (much better agreement with matched samples this way)
  pythia.readString("SpaceShower:pTdampMatch = 1");

  // Test ME shower corrections off, if desired
  //pythia.readString("TimeShower:MEcorrections = off");////

  // Shower-level HT trigger and other vetos
  MyUserHooks* myUserHooks = new MyUserHooks();
  pythia.setUserHooksPtr(myUserHooks);

  // Set up the process
  // *****  If you want to reset any BR's relative to the SLHA file, do so after the pythia.init() call *****
  bool external = false;
  pythia.readString("Beams:idA = 2212");  // proton...
  pythia.readString("Beams:idB = 2212");  // ...on proton
  pythia.readString("Beams:eCM = 13000");  // LHC13
  float mass_float = atof(mass.c_str());
  
    // external Les Houches Event file
    external = true;
    idLongLived = 9000006;//1000015;
    pythia.readString("Beams:frameType = 4"); 
    pythia.readString("Beams:LHEF = alp.lhe");

    //matching
    //pythia.readString("JetMatching:scheme = 1");
    //pythia.readString("JetMatching:setMad = off");
    //pythia.readString("JetMatching:qCut = 10.0");
    //pythia.readString("JetMatching:coneRadius = 1.0");
    //pythia.readString("JetMatching:etaJetMax = 10.0");
    //pythia.readString("JetMatching:nJetMax = 3");

  // Initialize Pythia
  pythia.init();

 

  // Open "output.dat" file
  ofstream outputFile;
  if (external)  outputFile.open((dir+"/output."+lheFileName+userTag+".dat").c_str());
  else           outputFile.open((dir+"/output."+lheFileName+userTag+".m"+mass+".dat").c_str());
 
  // Cross section, event counters
  double sigma = 0;
  int nEventsGenerated = 0;
  int nEventsPass = 0;

  // Allow for possibility of a few faulty events.
  int nAbort = 1000;
  int iAbort = 0;

  // Special counters to check Pythia treatment of R-hadron charges
  int nRhadronsProduced = 0;
  int nChargedRhadronsProduced = 0;

  // Analysis histogram (use if desired)
  Hist hist("up quark energy spectrum in gluino rest frame", 20, 0., atof(mass.c_str())/2);
  Hist hist_beta("heavy particle velocity spectrum", 100, 0.,1.);
  

  // Begin event loop; generate until none left in input file.     
  for (int iEvent = 0; iEvent < nGen ; ++iEvent) {

    if (!external && iEvent >= nGen)  break;

    //cout << iEvent << endl;
    //if (iEvent >= 10000)  break;

    // Generate events, and check whether generation failed.
    if (!pythia.next() || iEvent >= nHadronizeMax) {
      
      // If failure because reached end of file then exit event loop.
      if (pythia.info.atEndOfFile()) break; 

      // First few failures write off as "acceptable" errors, then quit.
      if (++iAbort < nAbort) continue;
      cout << "TOO MANY ERRORS:  BAILING OUT!" << endl;
      break;
    }


    nEventsGenerated++;
    nEventsPass++;

    // Particle holders
    vector<Particle> hardEventParticles;
    vector<Particle> HFhadrons;
    vector<Particle> finalStateParticles;
    vector<int>  finalStateParticleIndices;  // for debugging
    
    // What particle shall we set long-lived?
    int idLongLived_ = idLongLived;  // check global flag
    if (idLongLived_ == 0 && !Rhadrons && pythia.process.size() > 7) {  // if 0, decide who to keep stable on-the-fly based on hard event content
      bool isResonant = (pythia.process[6].mother1() == 5 && pythia.process[7].mother1() == 5);
      if ( isResonant && abs(pythia.process[6].id()) == abs(pythia.process[7].id()))  idLongLived_ = abs(pythia.process[6].id());
      if (!isResonant && abs(pythia.process[5].id()) == abs(pythia.process[6].id()))  idLongLived_ = abs(pythia.process[5].id());
      if (!isResonant && abs(pythia.process[5].id()) != abs(pythia.process[6].id()))  idLongLived_ = abs(pythia.process[5].id());  
    }
    //cout << idLongLived_ << endl;


    // Extract particles from the hard event
    for (int i = 0; i < pythia.process.size(); ++i) { 

      Particle part = pythia.process[i];

      // optional:  fill analysis histogram (energy of up-type quark in RPV gluino frame, to check if 3-body phase space is flat (linear rise in E))
      if (part.status()==23 && abs(part.id())==2) {
	      Vec4 moVec = pythia.process[part.mother1()].p();
	      ////hist.fill(part.p()*moVec/moVec.mCalc());
      }

      // pythia codes:  21: initial-state,  22: intermediate hard particle from user process,  23:  "final" hard particle from user process
      // "-" sign indicates particle has been decayed or annihilated
      bool hard = false;
      if (part.status() == -21) { 
	      hard = true;  
	      part.statusCode(1);   // will retain sign, setting status to -1, miming LHE conventions from MadGraph
      } 
      if (part.status() == -22) { 
	      hard = true;  
	      part.statusPos(); 
	      part.statusCode(2);   // also miming LHE, for primary unstable particle
      } 
      if (part.status() == 23)  {
	      hard = true;  
	      int mid = pythia.process[part.mother1()].id();
	      if (mid<0)  part.statusNeg();  else part.statusPos();
	      part.statusCode(abs(mid));  // departing from LHE convention, set the status to the mother ID
      }
      if (hard && part.status() != 90 && abs(part.status()) < 3e6) {  // status cutoff removes rare (and mysterious) glitches
	      hardEventParticles.push_back(part);
	      if (abs(part.id())==idLongLived && fabs(part.p().eta()) < 10.0)  hist_beta.fill(part.p().pAbs()/part.p().e());
	      continue;
      }
    }

    // First pass through the event-record:  Identify our long-lived particles immediately before they decay (e.g., after any showering),
    //  and assign their codes based on order of appearance.  Also build up a list of "post-shower, pre-fragmentation" partons, for
    //  matching to final-state hadrons if the long-lived particle isn't color-singlet.
    vector<int> longLivedIndices;
    vector<int> finalPartonIndices;

    for (int i = 1; i < pythia.event.size(); ++i) { 
      Particle part = pythia.event[i];
      if (abs(part.status()) < 70 && (abs(pythia.event[part.daughter1()].status()) > 70 || part.daughter1() == 0)) {
	      finalPartonIndices.push_back(i);  // post-shower / pre-fragmentation parton
      }
      if ( abs(part.id()) != idLongLived_ && !(Rhadrons && isRhadron(part.id())) )  continue;
      bool final = true;
      for (int j = part.daughter1(); j <= part.daughter2(); j++)  
	    if (abs(pythia.event[j].id())==idLongLived_ || (Rhadrons && isRhadron(pythia.event[j].id()))) { final=false; break; }
      if (!final) continue;
      longLivedIndices.push_back(i);
      if (part.colType() != 0)  coloredLongLived = true;  // catch if our would-be long-lived particles are actually colored...requires careful hadron->parton mapping
      if (isRhadron(part.id())) {  nRhadronsProduced++;  if (part.isCharged() != 0) nChargedRhadronsProduced++; }
    }
    
    // Extract final-state particles and b/c-hadrons
    vector<Vec4> rebuiltLongLived;  

    for (unsigned int i = 0; i < longLivedIndices.size(); i++)  rebuiltLongLived.push_back(Vec4(0,0,0,0));
    for (int i = 1; i < pythia.event.size(); ++i) {
      Particle part = pythia.event[i];

      if (part.id() == idLongLived)  {
        //double properTimeMin = minTransverseDecayLength * part.m() / part.pT() / speed_of_light;
        //double properTimeMax = maxTransverseDecayLength * part.m() / part.pT() / speed_of_light;
        //outputFile << " The LLP Mass is: " << sqrt(pow(part.e(),2)-pow(part.px(),2)-pow(part.py(),2)-pow(part.pz(),2)) << "\n" << endl;
        DisplacedVertex = lifetime*RestrictedExponential(properTimeMin/lifetime,properTimeMax/lifetime) * part.p()/ part.m() * speed_of_light; //in meters
        //cout << lifetime*RestrictedExponential(properTimeMin/lifetime,properTimeMax/lifetime)*speed_of_light << endl;
        //cout << "This is the boost of the LLP " << part.e()/part.m() << " and the mass is " << part.m() << endl;
      
      }
      // final-state particles
      if (part.isFinal())  {
        int ancestorIndex = getLongLivedAncestry(i,pythia,longLivedIndices,finalPartonIndices);
        part.statusCode(pythia.event[ancestorIndex].id());  // departing from LHE convention, set the status to the ancestry ID
	      //if (ancestor > 0)  rebuiltLongLived[ancestor-1] += part.p();
	      ////if (ancestor > 0)  cout << ancestor << endl;
	      finalStateParticles.push_back(part);
	      finalStateParticleIndices.push_back(i);
	      continue;
      }
     
    }
    
    
    //pythia.process.list();//////
    //pythia.event.list();////////
     
    // Output to text file
    outputFile << fixed << setprecision(4);
    /*
    outputFile << iEvent << " " << hardEventParticles.size() << endl;
    for (unsigned int i = 0; i < hardEventParticles.size(); i++) {
      Particle part = hardEventParticles[i];
      outputFile << part.id() << "  " << part.px() << " " << part.py() << " " << part.pz() << " " << part.e() << "  " << part.status() << endl;
    }
    */
    /*
    outputFile << "0 " << HFhadrons.size() << endl;
    for (unsigned int i = 0; i < HFhadrons.size(); i++) { 
      Particle part = HFhadrons[i];
      outputFile << part.id() << "  " << part.px() << " " << part.py() << " " << part.pz() << " " << part.e() << endl;
    }
    */
    //outputFile << "0 " << finalStateParticles.size() << endl;
    int displacedTrackCount = 0; 
    double jetHT = 0;

    for (unsigned int i = 0; i < finalStateParticles.size(); i++) {
      Particle part = finalStateParticles[i];
      if (part.isHadron())  jetHT += part.pT();
    }
    //outputFile << "The jet HT is: " << jetHT << endl;

    for (unsigned int i = 0; i < finalStateParticles.size(); i++) { 
      Particle part = finalStateParticles[i];

      if (part.status() == idLongLived && part.isHadron()){
        outputFile << part.id() << "  " << part.px() << " " << part.py() << " " << part.pz() << " " << part.e() << "  " << part.status() << "  " << DisplacedVertex.px() << " " << DisplacedVertex.py() << " " << DisplacedVertex.pz() << endl;
        
        double TransDis = sqrt(pow(DisplacedVertex.px(),2)+pow(DisplacedVertex.py(),2));
        
        if (part.pT() > 2.0 && TransDis < r2 && abs(part.eta()) < 1.0 && jetHT > HTcut){
          
          double q = part.id()/abs(part.id());
          double vx = DisplacedVertex.px(); double vy = DisplacedVertex.py();
          //cout << vx << " " << vy << "\n" << endl;
          double px = part.px(); double py = part.py();
          double rC = part.pT()/0.3/B; // in m

          double fpx = q*py;  double fpy = -q*px;   double fpt = sqrt(fpx*fpx+fpy*fpy);  fpx/=fpt;  fpy/=fpt;  // unit-perpendicular ("f") to momentum at vertex, pointing toward center-of-curvature
          double vCx = vx+rC*fpx;  double vCy = vy+rC*fpy;  // location of center-of-curvature
          double d = sqrt(vCx*vCx+vCy*vCy);   // distance from detector center to center-of-curvature

          double ip2D = fabs(d-rC);
          if (ip2D > 0.001) displacedTrackCount++;
        }  
      }
      /////outputFile << finalStateParticleIndices[i] << "  " << part.id() << "  " << part.px() << " " << part.py() << " " << part.pz() << " " << part.e() << "  " << part.status() << endl;
    }
    outputFile << "The displaced track count is: " << displacedTrackCount << endl;
    if (displacedTrackCount >= 2) {
      outputFile << "\n event is passed! \n" << endl;
      passedEvents++;
      }
    //for debugging
    /*
    outputFile << " \n Now comes all the particles \n" << endl;
    for (int i = 1; i < pythia.event.size(); ++i) { 
      Particle part = pythia.event[i];
      outputFile << i << " " << part.id() << "  " << part.px() << " " << part.py() << " " << part.pz() << " " << part.e() << " " << part.e()/part.m() <<  "  " << part.mother1() << " " << part.mother2() << " " << part.daughter1() << " " << part.daughter2() << endl;
    } 
    */
    outputFile << endl;
    

  // End of event loop.        
  }                                           

  outputFile << "\n The selection efficiency is :" << (float) passedEvents/(float) nGen << "\n" << endl;
  // Write "sigma.dat" file
  sigma =  pythia.info.sigmaGen()*1e12;
  ofstream sigmaFile;
  if (external)  sigmaFile.open((dir+"/sigma."+lheFileName+userTag+".dat").c_str());
  else           sigmaFile.open((dir+"/sigma."+lheFileName+userTag+".m"+mass+".dat").c_str());
  sigmaFile << sigma << " " << nEventsGenerated << " " << nEventsPass << endl;

  pythia.stat();

  // Optionally, write stats on R-hadrons
  cout << endl;
  cout << "# R-hadrons     : " << nRhadronsProduced << endl;
  cout << "fraction charged: " << nChargedRhadronsProduced*1./nRhadronsProduced << endl;

  // Optionally, display analysis histo
  //cout << hist;
  cout << hist_beta;

  // Done.                           
  return 0;
}
