#ifndef DISPLACEDLIB_H
#define DISPLACEDLIB_H

// FastJet
#include "fastjet/PseudoJet.hh"
#include "fastjet/ClusterSequence.hh"

#include<iostream> // needed for io
#include<fstream>  // file io
#include<sstream>  // needed for internal io
#include<vector> 


using namespace std;


/*----------------------------------------------------------------------------------

   displacedlib.h    (Brock Tweedie)

   Package for displaced particle decay analyses.

   Last "official" update:  15 Feb 2014

-------------------------------------------------------------------------------------*/



/////////////////////////////////////////////////////////////////////////////////
//
//                             GLOBAL PARAMETERS
//
/////////////////////////////////////////////////////////////////////////////////


// "true" constants
const double Pi = 3.14159265359;
const double halfPi = Pi/2.;
const double twoPi = 2.*Pi;
const double mZ = 91.2;
const double mW = 80.4;
const double mt = 173.;
const double speed_of_light = 3.e8;  // m/s

// basic reconstruction cuts
const double etaMaxTrack = 2.5;
const double ptMinLep = 20;

// empty 4-vector
const fastjet::PseudoJet blankJet(0.,0.,0.,0.);

// calorimeter model eta/phi grid and energy smearing parameters  **** NOT PRESENTLY IN USE
const double etaLim = 4.0;
const int nEtaHCAL = 80;
const int nPhiHCAL = 60;
const double dEtaHCAL = 2*etaLim/nEtaHCAL;
const double dPhiHCAL = 2*Pi/nPhiHCAL;
const int nECALperHCAL = 1;  ///////   ECAL and HCAL same size
const int nEtaECAL = nECALperHCAL*nEtaHCAL;
const int nPhiECAL = nECALperHCAL*nPhiHCAL;
const double dEtaECAL = 2*etaLim/nEtaECAL;
const double dPhiECAL = 2*Pi/nPhiECAL;
const bool ECALID = false;  // true: ECAL cell ID = 1000022, false: ECAL cell ID = 22
bool smearCAL = false;   // energy-smear the calorimeter cells, similar to Delphes

// calorimeter model physical barrel/endcap geometry (units in meters)
const double rECAL = 1.3;         // radius of inner ECAL barrel (CMS = 1.3)
const double zECAL = 2.9;         // half-length of inner ECAL endcap  (CMS = 2.9)
const double rHCAL = 1.8;         // radius of inner HCAL barrel in meters (CMS = 1.8)
const double zHCAL = 3.75;        // half-length of inner HCAL endcap (CMS = 3.75)
const double rMAX  = 2.8;         // outer radius of HCAL barrel (CMS = 2.8)
const double zMAX  = 5.5;         // outer half-length of HCAL endcap (CMS = 5.5)
const double rOuterECAL_ATLAS = 2.0;  // outer radius of ATLAS ECAL
const double rInnerECAL_ATLAS = 1.5;  // inner radius of ATLAS ECAL //Zhen
const double zOuterECAL_ATLAS = 4.0;  // outer half-length of ATLAS ECAL
const double rOuterHCAL_ATLAS = 4.25; // outer radius of ATLAS ECAL
const double zOuterHCAL_ATLAS = 6.0;  // outer half-length of ATLAS ECAL

// B-field
const double B    = 3.8;          // tracker field in tesla (CMS = 3.8, ATLAS = 2)
const double BATLAS = 2.0;

// tracker global variables
const double promptTrackingEfficiency = 0.9;

// flags and parameters for displacement
bool allowDisplacement = true;  // false => all vertices set to the geometric detector center
double zPrimaryVertex = 0;  // can reset the primary vertex displacement from nominal detector center event-by-event (~5cm RMS for 2012)
bool doIP3D = false;  // use numerical methods to compute 3D track impact parameters



////////////////////////////////////////////////////////////////////////////////
//
//                           FUNCTION DECLARATIONS
//
////////////////////////////////////////////////////////////////////////////////

// extra user data to be associated with final-state particles
class ParticleInfo : public fastjet::PseudoJet::UserInfoBase{
public:
  ParticleInfo() : _displacedCode(0), _v(blankJet){}
  ParticleInfo(const int & displacedCode, const fastjet::PseudoJet & v) :
   _displacedCode(displacedCode), _v(v){}
  int displacedCode() const { return _displacedCode;}
  fastjet::PseudoJet v() const { return _v; }
  void resetVertex(fastjet::PseudoJet & v) { _v = v; }
protected:
  int _displacedCode;     // which displaced particle decay (if any) did this come from?
  fastjet::PseudoJet _v;  // vertex
};

// extra user data to be associated with detector particles
class TrackInfo : public fastjet::PseudoJet::UserInfoBase{
public:
  TrackInfo() : _isTrack(false), _IP3D(0), _IP2D(0), _IP2Dpos(blankJet), _v(blankJet), _C(blankJet), _R(0), _index(-1){}
  TrackInfo(const bool & isTrack, const double & IP3D, const double & IP2D, const fastjet::PseudoJet & IP2Dpos, const fastjet::PseudoJet & v, const fastjet::PseudoJet & C, const double & R, const int & index) :
  _isTrack(isTrack), _IP3D(IP3D), _IP2D(IP2D), _IP2Dpos(IP2Dpos), _v(v), _C(C), _R(R), _index(index){}
  bool isTrack() const { return _isTrack;}
  double IP3D() const { return _IP3D; }
  double IP2D() const { return _IP2D; }
  fastjet::PseudoJet IP2Dpos() const { return _IP2Dpos; }
  fastjet::PseudoJet v() const { return _v; }
  fastjet::PseudoJet C() const { return _C; }
  double R() const { return _R; }
  int index() const { return _index; }
  void set_index(int index) {_index=index;}
protected:
  bool _isTrack;               // is it a track?
  double _IP3D;                // 3D impact parameter
  double _IP2D;                // 2D impact parameter
  fastjet::PseudoJet _IP2Dpos; // position of 2D impact
  fastjet::PseudoJet _v;       // true vertex
  fastjet::PseudoJet _C;       // transverse center-of-curvature
  double _R;                   // radius-of-curvature
  int    _index;               // an extra index (e.g., which jet this track belongs to)
};

// return uniform-distributed random #
double uniformRandom();

// return Poisson-distributed random #
int Poisson(double);

// return normally-distributed random #
double Normal();

// return exponentially-distributed random #
double Exponential();

// return exponentially-distributed random # within a specified range
double RestrictedExponential(double,double);

// determine the charge of a particle based on id
int charge(int);

// check whether a particle is a hadron, based on id
bool isHadron(int);

// calculate absolute phi distance
double Dphi(const fastjet::PseudoJet &, const fastjet::PseudoJet &);

// calculate signed phi distance
double signedDphi(const fastjet::PseudoJet &, const fastjet::PseudoJet &);

// calculate eta-phi distance
double DR(const fastjet::PseudoJet &, const fastjet::PseudoJet &);

// check if two 4-vectors are the same, up to some "safe" numerical precision
bool areSame(const fastjet::PseudoJet &, const fastjet::PseudoJet &);

// get total 3D length of a pseudojet (somehow not built into fastjet)
double pTot(const fastjet::PseudoJet &);

// sum a collection of pseudojets
fastjet::PseudoJet sumJets(const vector<fastjet::PseudoJet> &);

// scalar-sum pt's of a collection of pseudojets
double HT(const vector<fastjet::PseudoJet> &);

// 2012 CMS lepton ID efficiency, parametrized in CMS PAS SUS-13-010, appendix
double IDeff(int id, double pt);

// efficiency for picking up a track, as a function of pt, radius from the beamline, 2D impact parameter, and z
double trackingEfficiency(double,double,double,double);

// rotate clockwise around a point
void shiftRotateUnshift(double &, double &,double, double, double, double, double);

// find 3D impact parameter
double find3DimpactParameter(double, double, double, double);

// adjust 4-vector of a particle to point towards its impact point on the calorimeter
fastjet::PseudoJet repointParticle(const fastjet::PseudoJet &, const fastjet::PseudoJet &);

// absorb particles onto a "calorimeter" face or turn into "tracks", accounting for displaced vertices, B-field, and tracking efficiencies
void detectorize(const vector<fastjet::PseudoJet> &, vector<fastjet::PseudoJet> &);

// reconstruct particles as in the ATLAS displaced mu+tracks analyses
void getATLAStracks(const vector<fastjet::PseudoJet> &, vector<fastjet::PseudoJet> &);

// b-tagging and c-tagging
void bcTag(vector<fastjet::PseudoJet> &, const vector<fastjet::PseudoJet> &, double);



////////////////////////////////////////////////////////////////////////////
//
//                           FUNCTION DEFINITIONS
//
////////////////////////////////////////////////////////////////////////////


//---------------------------------------------
// return uniformly-distributed random #
//
double uniformRandom()
{
  return rand()*1./RAND_MAX;
}

//--------------------------------------------------
// return Poisson-distributed random #
//
// using algorithm from Wikipedia (Knuth)
//
int Poisson(double mu)
{
  double L = exp(-mu);
  int k = 0;
  double p = 1;
  while (p > L) {
    k++;
    double u = uniformRandom();
    p *= u;
  }
  return k-1;
}


//---------------------------------------------
// return normally-distributed random #
double Normal()
{
  double U = uniformRandom();
  double V = uniformRandom();
  return sqrt(-2*log(U))*cos(2*Pi*V);  // Box-Muller method
}

// return exponentially-distributed random #
double Exponential()
{
  return -log(uniformRandom());
}

// return exponentially-distributed random # within a specified range
double RestrictedExponential(double t1, double t2)
{
  double x1 = exp(-t1);
  double x2 = exp(-t2);
  return -log(x1 + (x2-x1)*uniformRandom() );
}


//------------------------------------------------------------------------------------------
// determine the charge of a particle based on id
//
//    *** the entire set of visible final state particles is
//         11 (e), 13 (mu), 22 (photon), 211 (pi^+/-), 321 (K^+/-), 130 (K_L),
//         310 (K_S)  (actually, I've never see this, but in principle, at large boost...)
//         2212 (p), 2112 (n) 
//
int charge(int id)
{
  if (id == -11 || id == -13 || id ==  211 || id ==  321 || id ==  2212)  return  1;
  if (id ==  11 || id ==  13 || id == -211 || id == -321 || id == -2212)  return -1;
  return 0;
}


//---------------------------------------------------------
// determine whether a particle is a hadron, based on id
//
bool isHadron(int id)
{
  int absid = abs(id);
  if (absid == 211 || absid == 321 || absid == 130 || absid == 310 || absid == 2212 || absid == 2112)  return true;
  return false;
}


//-----------------------------------------------------------------------------------
// determine the signed heavy quark ID (bottom or charm) inside of a heavy hadron
int HFID(int id)
{
  // note that I only store HF hadrons immediately before the heavy quark decays,
  // so don't worry about, e.g., radially-excited mesons which quickly release
  // pions/photons.  also, I don't independently store charms that came from bottom decay.
  if (id == 0)  return 0;
  int absid = abs(id);
  if (absid <= 10)  return id;  // bare quark
  int digit2 = ( absid-1000 *(absid/1000 ) - (absid%100 ) ) / 100 ;
  int digit3 = ( absid-10000*(absid/10000) - (absid%1000) ) / 1000;
  int flavor = max(digit2,digit3) * id/abs(id);
  if (absid < 1000 && abs(flavor) == 5)  flavor *= -1;  // meson codes are backward for down-type (e.g., 521 contains anti-b)
  return flavor;
}


//-----------------------------------------------
// calculate |\Delta phi| between two 4-vectors
//
double Dphi(const fastjet::PseudoJet & jet1, const fastjet::PseudoJet & jet2)
{
  double dphi = jet2.phi_std() - jet1.phi_std();
  if (dphi > Pi)  dphi -= 2*Pi;
  if (dphi < -Pi) dphi += 2*Pi;
  return fabs(dphi);
}


//----------------------------------------------------
// calculate signed \Delta phi between two 4-vectors
//
double signedDphi(const fastjet::PseudoJet & jet1, const fastjet::PseudoJet & jet2)
{
  double dphi = jet2.phi_std() - jet1.phi_std();
  if (dphi > Pi)  dphi -= 2*Pi;
  if (dphi < -Pi) dphi += 2*Pi;
 
  return dphi;
}


//-------------------------------------------
// calculate \Delta R between two 4-vectors
//
double DR(const fastjet::PseudoJet & jet1, const fastjet::PseudoJet & jet2)
{
  double dy = jet2.rap() - jet1.rap();
  double dphi = Dphi(jet1,jet2);
  return sqrt(dy*dy + dphi*dphi);
}


//----------------------------------------------------------------------------
// check if two 4-vectors are the same, up to some "safe" numerical precision
//
bool areSame(const fastjet::PseudoJet & jet1, const fastjet::PseudoJet & jet2)
{
  return (DR(jet1,jet2) < 0.001) && (fabs(jet1.perp()-jet2.perp()) < 0.001) &&
    (jet1.user_index() == jet2.user_index());
}


//----------------------------------------------------------------------
// get total 3D length of a pseudojet (somehow not built into fastjet)
//
double pTot(const fastjet::PseudoJet & jet)
{
  return sqrt(jet.px()*jet.px()+jet.py()*jet.py()+jet.pz()*jet.pz());
}


//------------------------------------
// sum a collection of pseudojets
//
fastjet::PseudoJet sumJets(const vector<fastjet::PseudoJet> & jetCollection)
{
  fastjet::PseudoJet sumJet = blankJet;
  for (int i = 0; i < jetCollection.size(); i++)  sumJet += jetCollection[i];
  return sumJet;
}


//-------------------------------------------------
// scalar-sum pt's of a collection of pseudojets
//
double HT(const vector<fastjet::PseudoJet> & jetCollection)
{
  double ptTot = 0;
  for (int i = 0; i < jetCollection.size(); i++)  ptTot += jetCollection[i].perp();
  return ptTot;
}


//-------------------------------------------------------------------------
// CMS lepton ID efficiency, parametrized in CMS PAS SUS-13-010, appendix
double IDeff(int id, double pt)
{
  id = abs(id);
  if (id==11)  return 0.91 - 6.83/pt + 78.1/pt/pt - 731/pt/pt/pt;
  if (id==13)  return 0.94 - 2.85/pt + 41.9/pt/pt - 227/pt/pt/pt;
  return 0;
}


//-------------------------------------------------
// efficiency for picking up a track, as a function of pt, radius from the beamline, 2D impact parameter, and z
//
double trackingEfficiency(double r, double pt, double ip2D, double zVertex)
{
  // This is based on a simple r^1 falloff model, roughly following fig 25 of CMS PAS TRK-09-001 (v0 draft).
  // Note that the efficiency for prompt tracks is not a very strong function of pT above ~1 GeV, but drops
  // quickly below that.  We model this as an abrupt cutoff.
  double rMax = 0.60;  // radius (in meters) at which the efficiency has fallen to 0
  double ptmin = 1.0;
  double ip2Dmax = 0.30;  // high IP tracks assumed lost, penalize in-between with another linearly falling function
  double zMax = 0.55;     // maximum vertex z that we'll consider (based on Fig 3 of EXO-12-037, displaced dilepton search)
  if (pt<ptmin || r>rMax || r<0 || ip2D>ip2Dmax || fabs(zVertex)>zMax) return 0;
  return promptTrackingEfficiency*(1-r/rMax)*(1-ip2D/ip2Dmax)*(1-fabs(zVertex)/zMax);
  /////return promptTrackingEfficiency*(1-r/rMax);
  /////return 1;///
}


//---------------------------------------------------
// rotate clockwise around a point
void shiftRotateUnshift(double & xNew, double & yNew, double xOld, double yOld, double xC, double yC, double angle)
{
  // New = R*(Old-C) + C = R*Old + (1-R)*C
  xNew =  xOld*cos(angle) + yOld*sin(angle)  +  xC*(1-cos(angle)) + yC*(0-sin(angle));
  yNew = -xOld*sin(angle) + yOld*cos(angle)  +  xC*(0+sin(angle)) + yC*(1-cos(angle));
}


//----------------------------------------------------
//  compute the 3D impact parameter of a track.  note that the result is independent of electric charge = +/-1.
//      r = track radius of curvature
//      d = distance between beamline and center-of-curvature
//      z0 = z-distance to primary vertex (signed) when particle is at its point of closest transverse approach to the beamline
//      pz/pt = ratio between pz and pt (signed)
double find3DimpactParameter(double r, double d, double z0, double pzpt)
{
  // we consider the 3D distance to the primary vertex, as a function of the helix wind phi = [-inf,inf].  this function generally
  // has multiple local minima.  if pz != 0, there is a unique minimum.  to find it, we'll find the roots of the derivative
  // of the squared-distance, with a common factor of 2r divided out.  this is equivalent to finding the points of intercept
  // between a sine-wave and a line.
  // WARNING:  since this method is based on an iterative approximation, there is no guarantee that it will actually find the
  //           true minimimum (though we certainly try to help it along!)
  double ip3D = sqrt((d-r)*(d-r)+z0*z0);   // initialize to 3D distance of 2D closest approach
  if (pzpt==0)  return ip3D;  // vanishing pz
  if (r==0)  return d;   // vanishing pt
  const int nWindsMax = 1;  // control how many winds we search away from the starting position
  const int nSeeds = 4;   // how many seed phi's shall we try within each wind?
  const int nIterationsMax = 15;  // control how many numerical updates we apply
  const double rPhiTolerance = 1e-6;  // when numerical iterations change r*phi by less than this amount (~micron), stop iterating 
  double coeffSin = d;          // coefficient of sine term (must be positive)
  double coeff0 = z0*pzpt;      //             ...0th-order term in phi
  double coeff1 = pzpt*pzpt*r;  //             ...1st-order term in phi
  double linearXintercept = -coeff0/coeff1;  // find phi at which linear piece passes 0, this will seed our search
  double ipCurv = 0;    // initialize the candidate minimum's curvature vs phi (as a cross check, shouldn't be positive)
  for (int n = -nWindsMax; n <= nWindsMax; n++) {
    double phiMin = 2*Pi*n;  double phiMax = 2*Pi*(n+1);
    double linearLeft = coeff0 + coeff1*phiMin;  double linearRight = coeff0 + coeff1*phiMax;
    double linearAvg = (linearLeft+linearRight)/2;
    double linearAbsMinBoundary = min(fabs(linearLeft),fabs(linearRight));
    if (linearAbsMinBoundary > coeffSin && linearLeft*linearRight > 0)  continue;   // no chance to find root
    for (int seedIt = 0; seedIt < nSeeds; seedIt++) {
      double phiTest = phiMin + (seedIt+0.5)*twoPi/nSeeds;
      double dfTest=0;  // derivative of derivative (propto second derivative)
      for (int i = 0; i < nIterationsMax; i++) { 
	double fTest = coeffSin*sin(phiTest) + coeff0 + coeff1*phiTest;  
	dfTest = coeffSin*cos(phiTest) + coeff1;
	///cout << "i = " << i << ", phiTest = " << phiTest << ", 2r*fTest = " << fTest*2*r << ", dphi = " << -fTest/dfTest << endl;
	///////cout << "   r*dphi = " << fabs(r*fTestA/dfA) << endl;
	if (dfTest == 0 || fabs(r*fTest/dfTest) < rPhiTolerance) break;
	phiTest -= fTest/dfTest;  // update by Newton's method
      }
      double ip3DTest = sqrt((d*d+r*r+z0*z0) - 2*d*r*cos(phiTest) + 2*z0*pzpt*r*phiTest + pzpt*pzpt*r*r*phiTest*phiTest);
      if (ip3DTest < ip3D) { ip3D = ip3DTest; ipCurv = dfTest; }
      //////cout << "   n = " << n << endl;
      /////cout << "   candidate ip3D = " << ip3DTest << endl;
      //////cout << "   2nd derivative = " << dfTest << endl;
    }
  }

  if (ipCurv < 0)  cout << "WARNING: Identified ip3D minimum has negative second derivative vs helix wind" << endl;

  return ip3D;
}


//---------------------------------------------------
// adjust 4-vector of a particle to point towards its impact point on the calorimeter
//
fastjet::PseudoJet repointParticle(const fastjet::PseudoJet & part, const fastjet::PseudoJet & hit)
{
  fastjet::PseudoJet new_part(hit.px(),hit.py(),hit.pz(),1);
  double p = pTot(part);
  double hitDistance = pTot(hit);
  new_part *= p/hitDistance;
  new_part.reset(new_part.px(),new_part.py(),new_part.pz(), part.e());
  new_part.set_user_index(part.user_index());
  new_part.set_user_info(new TrackInfo());  // default, blank track info (not a track!)
  /*  // for debugging
  double rCAL = rECAL;  double zCAL = zECAL;
  cout << endl;
  cout << "impact at  (" << hit.px()/rCAL << ", " << hit.py()/rCAL << ", " << hit.pz()/zCAL << ")" << endl;
  cout << "momentum  (" << part.px()/p << ", " << part.py()/p << ", " << part.pz()/p << ")  ->  (" << new_part.px()/p << ", " << new_part.py()/p << ", " << new_part.pz()/p << ")" << endl; */
  
  return new_part;
}


//----------------------------------------------------
// Apply a very basic detector model.  This is meant to be a strawman for CMS particle-flow, carefully accounting only for effects like 
// the B-field, vertex displacements, and tracking efficiency (as a function of radius).  Input a list of particles and their vertices.  
// Output a list of particles with the same energies as the originals, but either
//    1) with momentum vectors pointing from the detector center to their impact point on the calorimeter face
// or 2) with momentum vectors evaluated at the point of closest approach to the beamline
// Note that *no* energy or position smearing is applied.  (You should smear particle/jet energies and directions yourself, if you want.
// You may also consider smearing vertexing parameters for tracks.)
// 
void detectorize(const vector<fastjet::PseudoJet> & particles, vector<fastjet::PseudoJet> & detector_particles)
{
  // id codes for untracked particles that have been absorbed into the calorimeters
  int HCALid = 1;
  int ECALid = 22;
  
  detector_particles.clear();

  for (int i = 0; i < particles.size(); i++) {
 
    // basic setup
    fastjet::PseudoJet part = particles[i];
    fastjet::PseudoJet vert = part.user_info<ParticleInfo>().v();
    if (!allowDisplacement) { vert = blankJet; zPrimaryVertex = 0; }
    int id = part.user_index();

    /* ///// For debugging ////
    double ptest = 100000 * (0.15*B*rECAL);
    part.reset(ptest,ptest,-ptest/10,0);
    part.reset(part.px(),part.py(),part.pz(),pTot(part));
    vert.reset(0,rECAL/4,0,0);
    id = -11;
    zPrimaryVertex = 0; */
    /////////////////
    
    double px = part.px();  double py = part.py();  double pz = part.pz();  double E = part.e();
    double pt = part.pt();   double p = pTot(part);
    double vx = vert.px();  double vy = vert.py();  double vz = vert.pz();  // units are assumed to be in *meters*
    double vt = vert.pt();
    double Bmin = 1e-5;   // need some nonzero B-field for the algorithm to work
    double Beff = B;   double rCAL = rHCAL;  double zCAL = zHCAL;  int CALid = HCALid;  int q = charge(id); // default, assume charged hadron
    bool track = true;
    if (fabs(Beff) < Bmin)  Beff = Bmin;
    if (id==22)              { Beff=Bmin; rCAL=rECAL; zCAL=zECAL; CALid=ECALid; q=1; track=false; }  // photon
    else if (charge(id)==0)  { Beff=Bmin; q=1; track=false; }  // neutral hadron (could also be neutralinos, etc...regardless don't bend)
    if (abs(id)==11)         { rCAL=rECAL; zCAL=zECAL; CALid=ECALid; }  // electron
    if (abs(id)==13)         { Beff=Bmin; rCAL=10; zCAL=1e6; CALid=id; }  // muon (don't bend..doesn't enter calorimeter)
    double ptcrit = (0.15)*Beff*rCAL;   // critical pt for reaching calorimeter from non-displaced vertex  (0.15 comes from c/2 in MKS units)
    double rC = part.pt()/ptcrit*rCAL/2;  // track radius-of-curvature
    if (rC==0)  continue;  // skip if vanishing pt (lost along the beamline)

    // if the vertex is inside the body of a calorimeter, just deposit the energy there. 
    // ECAL can absorb only electrons and photons, HCAL can absorb anything except muons and exotics.
    if ( ( (id==22 || abs(id)==11) && (vt>rECAL && vt<rMAX && fabs(vz)<zECAL || fabs(vz)>zECAL && fabs(vz)<zMAX && vt<rMAX) ) ||
	 (      isHadron(id)       && (vt>rHCAL && vt<rMAX && fabs(vz)<zHCAL || fabs(vz)>zHCAL && fabs(vz)<zMAX && vt<rMAX) ) ) {
      fastjet::PseudoJet new_part = repointParticle(part,vert);
      if (vt<rHCAL && fabs(vz)<zHCAL)  new_part.set_user_index(ECALid);
      else                             new_part.set_user_index(HCALid);
      detector_particles.push_back(new_part);
      continue;
    }

    // if the vertex is outside the entire calorimeter body, just skip it
    // ** note that this misses cases where
    //     1) the particle is pointing back toward the HCAL and fated to hit it on the outside surface
    //     2) the particle gets picked up as a "muon" or otherwise falls into ATLAS's muon tracker activity analysis
    // you'd have to be extremely careful here, since the solenoid (at CMS) is immediately outside the HCAL, and therefore the field
    // strength and uniformity change dramatically.
    if (vt>rMAX || fabs(vz)>zMAX)  continue;

    // Note on B-field:
    // My (arbitrary) convention is that positive charge curves in a clockwise arc.  I.e., transverse momentum pointing toward +yHat will
    // be deflected toward +xHat.  This means that the phi-of-impact on the CAL will tend to be negatively displaced (ignoring vertex
    // displacement).

    // set up track geometry
    double fpx = q*py;  double fpy = -q*px;   double fpt = sqrt(fpx*fpx+fpy*fpy);  fpx/=fpt;  fpy/=fpt;  // unit-perpendicular ("f") to momentum at vertex, pointing toward center-of-curvature
    double vCx = vx+rC*fpx;  double vCy = vy+rC*fpy;  // location of center-of-curvature
    double d = sqrt(vCx*vCx+vCy*vCy);   // distance from detector center to center-of-curvature
    double eCx = vCx/d;  double eCy = vCy/d;  // unit-vector ("e") pointing from detector center to center-of-curvature (think of as "x-axis")
    double fCx = -eCy;  double fCy = eCx;  // unit-perpendicular ("f"), rotated +90-degrees (think of as "y-axis")
    double dvx = vx-vCx;  double dvy = vy-vCy;  // production vertex coords relative to center-of-curvature

    // if it can be tracked, replace by vector at point of closest approach to the beamline, compute impact parameters
    // (tracks implicitly keep their original particle id's)
    double ip2D = fabs(d-rC);  // transverse impact parameter
    track = (track && uniformRandom()<trackingEfficiency(vt,pt,ip2D,vert.pz()) && fabs(part.rapidity())<etaMaxTrack);
    if (track) {
      fastjet::PseudoJet new_part(q*fCx*pt,q*fCy*pt,pz,E);
      double ipx = (d-rC)*eCx;  double ipy = (d-rC)*eCy;  // coordinates of closest transverse approach
      double dipx = ipx-vCx;  double dipy = ipy-vCy;  // ...relative to center-of-curvature
      double arc = acos( (dvx*dipx+dvy*dipy)/rC/rC);  // angle around center-of-curvature between ip and v 
      if (!(arc >= 0 || arc < 0))  arc = 0;  // catch spurious NaN from rounding errors (common for prompt tracks)
      double arcsign = asin( (dvx*dipy-dvy*dipx)/rC/rC );
      if (arcsign != 0)  arcsign = arcsign/fabs(arcsign);  else  arcsign = 1;  // clockwise from ip->v is "positive"
      double ipz = vz - pz/pt*q*arcsign*arc*rC;  // z-displacement at 2D ip, propagating backward/forward with "minimal wind"
      ipz -= zPrimaryVertex;  // correct for any z-displacement of primary vertex
      double ip3D = sqrt(ip2D*ip2D+ipz*ipz);  // default guess for ip3D (often an overestimate)
      if (pz != 0 && ip2D > 1e-6 && doIP3D)   ip3D = find3DimpactParameter(rC,d,ipz,pz/pt);  // refined numerical ip3D (don't bother if ip2D is already tiny, like sub-micron)
      new_part.set_user_info(new TrackInfo(true,ip3D,ip2D,fastjet::PseudoJet(ipx,ipy,0,0),vert,fastjet::PseudoJet(vCx,vCy,0,0),rC,-1));
      new_part.set_user_index(id);
      detector_particles.push_back(new_part);
      /* // for debugging
      cout << endl;
      cout << "track  (" << part.px()/p << ", " << part.py()/p << ", " << part.pz()/p << ")  ->  (" << new_part.px()/p << ", " << new_part.py()/p << ", " << new_part.pz()/p << ")" << endl;
      cout << "   ip3D/rCAL = " << ip3D/rCAL << "   ip3Dnaive/rCAL = " << sqrt(ip2D*ip2D+ipz*ipz)/rCAL << endl;
      cout << "   ip2D/rCAL = " << ip2D/rCAL << endl; */
      continue;
    }
    if (!track && abs(id)==13)  continue;  // skip muons that couldn't be tracked (note: untracked electrons get turned into "photons")

    // find the coordinates for CAL barrel intercept
    double dHit = (d*d + rCAL*rCAL - rC*rC)/(2*d);  // signed-distance along line separating CAL center and center-of-curvature at which a perpendicular line would touch the two circles' intercept
    if (fabs(dHit) > rCAL) {  // spiral-out...propagate into endcap
      if (pz==0) continue;  // ignore if spiraling indefinitely without propagating in z  
      double zHitEndcap = pz/fabs(pz) * zCAL;
      double arc = q * pt/fabs(pz) * fabs(zHitEndcap-vz) / rC;  // amount of clockwise angle subtended (can be > pi)
      double xHitEndcap,yHitEndcap;
      shiftRotateUnshift(xHitEndcap,yHitEndcap,vx,vy,vCx,vCy,arc);
      fastjet::PseudoJet new_part = repointParticle(part,fastjet::PseudoJet(xHitEndcap,yHitEndcap,zHitEndcap,1));
      new_part.set_user_index(CALid);
      detector_particles.push_back(new_part);
      ////cout << new_part.pt() << endl;
      // cout << "spiral-out:  arc = " << arc << endl; // for debugging
      continue;
    }
    double perpHit = q*sqrt(rCAL*rCAL - dHit*dHit);
    double xHit = dHit*eCx + perpHit*fCx;  double yHit = dHit*eCy + perpHit*fCy; 

    // determine the arclength traversed between vertex and CAL barrel impact, and the z-intercept
    double dxHit = xHit-vCx;  double dyHit = yHit-vCy;
    double arclength = rC * acos( (dvx*dxHit+dvy*dyHit)/rC/rC );  // arc can only span 0->pi if not spiral-out
    if (!(arclength >= 0 || arclength < 0))  arclength = rCAL;  // catch spurious NaN from rounding errors (common for prompt particles)
    double zHit = vz + (pz/pt)*arclength;

    // if the z-intercept would be beyond the barrel, stop it at the endcap
    bool endcap = false;  // flag endcap particles in case we want to do cross-check studies below
    if (fabs(zHit) > zCAL) {
      endcap = true;
      double zEndcap = zHit/fabs(zHit) * zCAL;
      double zFrac = fabs((zEndcap-vz)/(zHit-vz));  // fraction of z completed = fraction of helix completed
      double arc = q*arclength/rC;  // amount of clockwise angle subtended about the center-of-curvature before we would have hit the barrel
      double Darc = -(1-zFrac)*arc;  // amount of angle to subtract, to account for the fact that we hit endcap first
      double xHitEndcap,yHitEndcap;
      shiftRotateUnshift(xHitEndcap,yHitEndcap,xHit,yHit,vCx,vCy,Darc);
      xHit = xHitEndcap;
      yHit = yHitEndcap;
      zHit = zEndcap;
    }

    // new 4-vector:  same |momentum| and energy, but pointing toward the impact point on the CAL barrel inner face
    fastjet::PseudoJet new_part = repointParticle(part,fastjet::PseudoJet(xHit,yHit,zHit,1));
    new_part.set_user_index(CALid);
    detector_particles.push_back(new_part);
    //cout << "id = " << id << ",  cal = " << endcap << ",  pt = " << new_part.pt() << ", (original pt = " << part.pt() << ")" << endl; // for debugging
 
    /*
    // alternative method with vertices set to detector center, for cross-checks (you can comment this out if not using)
    double phiShift = q*asin(ptcrit/pt);  // phi offset around the barrel (*not* arc-length around center-of-curvature, but proportional)
    double xHitAlt = (px*cos(phiShift) + py*sin(phiShift)) * rCAL/pt;
    double yHitAlt = (py*cos(phiShift) - px*sin(phiShift)) * rCAL/pt;
    double zHitAlt = rCAL * pz/ptcrit * fabs(phiShift);
    bool endcapAlt = false;
    if (fabs(zHitAlt) > zCAL) {  // hit endcap
      endcapAlt = true;
      double zEndcap = zHitAlt/fabs(zHitAlt) * zCAL;
      double zFrac = fabs(zEndcap/zHitAlt);
      double phiShift_ = zFrac*phiShift;  // (angle around center-of-curvature and deflection from straight line are proportional)
      double rCAL_ = rCAL*(pt/ptcrit)*sin(q*phiShift_);  // stops at a smaller radius  
      xHitAlt = (px*cos(phiShift_) + py*sin(phiShift_)) * rCAL_/pt;
      yHitAlt = (py*cos(phiShift_) - px*sin(phiShift_)) * rCAL_/pt;
      zHitAlt = zEndcap;
    }
    fastjet::PseudoJet new_partAlt(xHitAlt,yHitAlt,zHitAlt,1);
    new_partAlt *= p/sqrt(new_partAlt.px()*new_partAlt.px()+new_partAlt.py()*new_partAlt.py()+new_partAlt.pz()*new_partAlt.pz());
    new_partAlt.reset(new_partAlt.px(),new_partAlt.py(),new_partAlt.pz(),E);
    new_partAlt -= new_part;  // subtract nominal effective 4-vector calculation and compare
    if (!track) {
      cout << endcap << endcapAlt << "    " << new_partAlt.px() << "  " << new_partAlt.py() << "  " << new_partAlt.pz() << endl;
      cout <<  "      " << xHitAlt-xHit << "  " << yHitAlt-yHit << "  " << zHitAlt-zHit << endl;
    }
    */

  }

  return;
}



//----------------------------------------------------
//  Setup for ATLAS displaced mu+tracks style analysis
//  Applies tracker efficiency and does basic geometry.  Since displacements are modest, and jets are
//  not an essential feature of the analysis, the fancy CMS-style calorimeter geometry and particle propagation isn't used.
// 
void getATLAStracks(const vector<fastjet::PseudoJet> & particles, vector<fastjet::PseudoJet> & ATLAStracks)
{
  // * muon-triggered...get reco efficiency...maybe driven by inner detector reco
  // * track pT > 1 GeV
  
  ATLAStracks.clear();

  for (int i = 0; i < particles.size(); i++) {
 
    // basic setup
    fastjet::PseudoJet part = particles[i];
    fastjet::PseudoJet vert = part.user_info<ParticleInfo>().v();
    if (!allowDisplacement) { vert = blankJet; zPrimaryVertex = 0; }
    int id = part.user_index();

    // keep only long-lived charged particles
    if (charge(id)==0)  continue;
    
    double px = part.px();  double py = part.py();  double pz = part.pz();  double E = part.e();
    double pt = part.pt();   double p = pTot(part);
    double vx = vert.px();  double vy = vert.py();  double vz = vert.pz();  // units are assumed to be in *meters*
    double vt = vert.pt();
    double Bmin = 1e-5;   // need some nonzero B-field for the algorithm to work
    double Beff = BATLAS;   double rCAL = 1.0;  int q = charge(id);
    bool track = true;
    if (fabs(Beff) < Bmin)  Beff = Bmin;
    double ptcrit = (0.15)*Beff*rCAL;   // ....holdover from CMS-style analysis, necessary to get rC with minimal code modification
    double rC = part.pt()/ptcrit*rCAL/2;  // track radius-of-curvature
    if (rC==0)  continue;  // skip if vanishing pt (lost along the beamline)

    // if the vertex is in a dead/unusable region, ignore the track (using Fig 3 of ATLAS-CONF-2013-092)
    if (fabs(vz) > 0.30)  continue;
    if (vt > 0.18 || (0.12 < vt && vt < 0.13) || (0.085 < vt && vt < 0.095) || (0.045 < vt && vt < 0.060) || (0.025 < vt && vt < 0.038))  continue;

    // Note on B-field:
    // My (arbitrary) convention is that positive charge curves in a clockwise arc.  I.e., transverse momentum pointing toward +yHat will
    // be deflected toward +xHat.

    // set up track geometry
    double fpx = q*py;  double fpy = -q*px;   double fpt = sqrt(fpx*fpx+fpy*fpy);  fpx/=fpt;  fpy/=fpt;  // unit-perpendicular ("f") to momentum at vertex, pointing toward center-of-curvature
    double vCx = vx+rC*fpx;  double vCy = vy+rC*fpy;  // location of center-of-curvature
    double d = sqrt(vCx*vCx+vCy*vCy);   // distance from detector center to center-of-curvature
    double eCx = vCx/d;  double eCy = vCy/d;  // unit-vector ("e") pointing from detector center to center-of-curvature (think of as "x-axis")
    double fCx = -eCy;  double fCy = eCx;  // unit-perpendicular ("f"), rotated +90-degrees (think of as "y-axis")
    double dvx = vx-vCx;  double dvy = vy-vCy;  // production vertex coords relative to center-of-curvature

    // if it can be tracked, replace by vector at point of closest approach to the beamline, compute impact parameters
    // (tracks implicitly keep their original particle id's)
    double ip2D = fabs(d-rC);  // transverse impact parameter
    if (uniformRandom() > 0.85*(1-vt/0.24)*(1-fabs(vz)/0.30))  continue;   // track reco efficiency (fudge) factor
    track = (track && fabs(part.rapidity())<etaMaxTrack && part.pt() > 1.0);
    if (track) {
      fastjet::PseudoJet new_part(q*fCx*pt,q*fCy*pt,pz,E);
      double ipx = (d-rC)*eCx;  double ipy = (d-rC)*eCy;  // coordinates of closest transverse approach
      double dipx = ipx-vCx;  double dipy = ipy-vCy;  // ...relative to center-of-curvature
      double arc = acos( (dvx*dipx+dvy*dipy)/rC/rC);  // angle around center-of-curvature between ip and v 
      if (!(arc >= 0 || arc < 0))  arc = 0;  // catch spurious NaN from rounding errors (common for prompt tracks)
      double arcsign = asin( (dvx*dipy-dvy*dipx)/rC/rC );
      if (arcsign != 0)  arcsign = arcsign/fabs(arcsign);  else  arcsign = 1;  // clockwise from ip->v is "positive"
      double ipz = vz - pz/pt*q*arcsign*arc*rC;  // z-displacement at 2D ip, propagating backward/forward with "minimal wind"
      ipz -= zPrimaryVertex;  // correct for any z-displacement of primary vertex
      double ip3D = sqrt(ip2D*ip2D+ipz*ipz);  // default guess for ip3D (often an overestimate)
      if (pz != 0 && ip2D > 1e-6 && doIP3D)   ip3D = find3DimpactParameter(rC,d,ipz,pz/pt);  // refined numerical ip3D (don't bother if ip2D is already tiny, like sub-micron)
      new_part.set_user_info(new TrackInfo(true,ip3D,ip2D,fastjet::PseudoJet(ipx,ipy,0,0),vert,fastjet::PseudoJet(vCx,vCy,0,0),rC,-1));
      new_part.set_user_index(id);
      ATLAStracks.push_back(new_part);
      continue;
    }
  }


  return;
}


//-----------------------------------
// b-tagging and c-tagging
//
void bcTag(vector<fastjet::PseudoJet> & jets, const vector<fastjet::PseudoJet> & bcHadrons, double Rtag)
{
  // first, find the unique closest jet to each b hadron
  vector<int> jetIndices;
  for (unsigned int itb = 0; itb < bcHadrons.size(); itb++) {
    double DRmin = 100000.;
    int indexMin = -1;
    for (unsigned int itj = 0; itj < jets.size(); itj++) {
      if (jets[itj].perp() < 1.0)  continue;  // don't try to match to "blank" jets
      double DRlocal = DR(bcHadrons[itb],jets[itj]);
      if (DRlocal < DRmin)  {  DRmin = DRlocal;  indexMin = itj; }
    }
    if (DRmin < Rtag)   jetIndices.push_back(indexMin);
    else                jetIndices.push_back(-1);
  }

  // next, check for hard b/c hadrons matched to each jet (pick off the hardest flavor)
  for (unsigned int itj = 0; itj < jets.size(); itj++) {
    double ptLocal;
    double ptMax = 0;
    int idLead = 1;  // 1 = unflavored (default)
    for (unsigned int itb = 0; itb < jetIndices.size(); itb++) {
      if (jetIndices[itb] == itj) {
	ptLocal = bcHadrons[itb].perp();
	if (ptLocal > ptMax)  { ptMax = ptLocal; idLead = HFID(bcHadrons[itb].user_index()); }
      }
    }
    jets[itj].set_user_index(idLead);
  }
}





#endif
