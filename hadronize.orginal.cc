//
//
//  Hadronize parton-level events from an LHE file, or generate directly in Pythia.  It switches to specific internal
//  processes when fed certain keys at the command-line (see below).
//
//
//  This version appends a code to each final-state particle, to indicate which primary particle they come from (if any).
//  You can also include an HT-trigger to save time.
//
//
//    Last official "update":  06 Feb 2014

/////// NOTE: CURRENTLY ID=35 IS SET TO LONG-LIVED FOR TESTS OF DECAYS OF THE HIGGS BOSON INTO LONG-LIVED SCALARS


//#include "Pythia8/Pythia.h"
#include "/home/zhen/pythia8186/include/Pythia8/Pythia.h"
using namespace Pythia8; 


// which particle do we treat as long-lived?
int idLongLived = 35;   // if 0, looks for first particle of 2->2 pair in event record, excepting intermediate resonances
                       // if negative, effectively deactivated
                       // if you specify one of the internal generation processes at the command line, this gets reset
bool coloredLongLived;  // flag indicating if our would-be long-lived particles are actually colored (set below)


// shower-level trigger parameters
const double HTmin = 0;  // don't set higher than than the final jet-level HT trigger
const double showerPtInterrupt = 30;  // pT scale at which to interrupt the shower and check HT (should be comparable to minimum jet pT)

// SPECIAL:  veto emissions above a given pT scale, for poor-man's matching studies
const double pTshowerMax = -30;

// cap # of events to hadronize, if desired
const int nHadronizeMax = 1e9;

// global pointer to the pythia object
Pythia * pythiaPtr;

string xmldir = "/Users/brock/pythia8176/xmldoc";   //////   SET TO YOUR PYTHIA8 DIRECTORY   
string slhafile = "/Users/brock/Displaced/slha_input.spc";
string slhafileStop = "/Users/brock/Displaced/slha_input.spc";
string slhafileDynamicalRPV = "/Users/brock/Displaced/slha_input.DynamicalRPV.spc";
string slhafileMiniSplit = "/Users/brock/Displaced/slha_input.MiniSplit.spc";
string slhafileGMSBHiggsinoZ = "/Users/brock/Displaced/slha_input.GMSBHiggsino.Z.spc";
string slhafileGMSBHiggsinoh = "/Users/brock/Displaced/slha_input.GMSBHiggsino.h.spc";
string slhafileRPVHiggsino = "/Users/brock/Displaced/slha_input.RPVHiggsino.spc";
string slhafileGMSBsquarkgluino = "/Users/brock/Displaced/slha_input.GMSBsquark+gluino.spc";
string slhafileATLASneutralino = "/Users/brock/Displaced/slha_input.ATLASneutralino.spc";  // for ATLAS mu+tracks calibration
string slhafileDisplacedSUSY = "/Users/brock/Displaced/slha_input.DisplacedSUSY.spc";  // for CMS displaced e+mu calibration


// list of R-hadron id codes
// R-hadrons must be treated specially, since they do not have a unique id
bool Rhadrons = false;
const int nRhadrons = 31;
int RhadronIDs[nRhadrons] = {1000993,1009113,1009213,1009223,1009313,1009323,1009333,1091114,1092114,1092214,1092224,1093114,1092314,1093224,1093314,1093324,1093334,1000612,1000622,1000632,1000642,1000652,1006113,1006211,1006213,1006223,1006311,1006313,1006321,1006323,1006333};
bool isRhadron(int id)
{
  for (int i = 0; i < nRhadrons; i++)  if (abs(id) == RhadronIDs[i])  return true;
  return false;
}


// determine heaviest flavor inside of a hadron
int getFlavor(int id) {
  int absid = abs(id);
  int digit2 = (absid - 1000*(absid/1000) - absid%100)/100;
  int digit3 = (absid - 10000*(absid/10000) - absid%1000)/1000;
  return max(digit2,digit3);
}

// my own simple particle definition
struct myParticle {
  int    id;
  double px;
  double py;
  double pz;
  double e;
  int    stat;
};


// "UserHooks" class for vetoing events at shower stage, before hadronization.
// Currently, veto on HT, after a few showering steps.
//
class MyUserHooks : public UserHooks {

public:

  MyUserHooks() {}
  ~MyUserHooks() {}
  
  virtual bool canVetoPT() {return true;}    // Allow a veto for the interleaved evolution in pT.
  virtual double scaleVetoPT() {return showerPtInterrupt;}    // Do the veto test at a specified pT scale.

  // Access the event in the interleaved evolution.
  virtual bool doVetoPT(int iPos, const Event& event) {

    // iPos <= 3 for interleaved evolution; skip others.
    if (iPos > 3) return false;

    // Extract a copy of the partons in the hardest system.
    subEvent(event);
    double HTj = 0;
    for (int i = 0; i < event.size(); ++i) { 
      Particle part = event[i];
      if (part.isFinal())  HTj += part.pT();
    }
    
    // Run the HT trigger
    if (HTj < HTmin) return true;     // Veto events which do not have enough "jet"-HT.

    return false;    // Do not veto events that got this far.
  }

  /*
  // Experimental, approximate "matching" by vetoing high-kT FSR emission inside of particle decays
  virtual bool canVetoFSREmission() {return true;}  // Allow emission veto
  virtual bool doVetoFSREmission( int sizeOld, const Event& event, int iSys, bool inResonance = false) {
    if (inResonance && pTshowerMax > 0) {
      double kTmin = 1e10;
      int nPart = pythiaPtr->partonSystems.sizeOut(iSys);
      if (nPart != 4)  return false;  //  only apply emission veto after the shower has operated once on the ul -> u g n1 decay
      for (int iMem = 0; iMem < nPart; iMem++) {
	int iPos = pythiaPtr->partonSystems.getOut(iSys,iMem);
	for (int jMem = iMem+1; jMem < nPart; jMem++) {
	  int jPos = pythiaPtr->partonSystems.getOut(iSys,jMem);
	  int id_i = event[iPos].id();
	  int id_j = event[jPos].id();
	  if (event[iPos].colType()==0 || event[jPos].colType()==0)  continue;
	  if (!(id_i==21 || id_j==21 || id_i==-id_j))  continue;
	  Vec4 p_i = event[iPos].p();
	  Vec4 p_j = event[jPos].p();
	  double pT_i = p_i.pT();
	  double pT_j = p_j.pT();
	  double eta_i = 0.5*log( (p_i.pAbs() + p_i.pz()) / (p_i.pAbs() - p_i.pz()) );
	  double eta_j = 0.5*log( (p_j.pAbs() + p_j.pz()) / (p_j.pAbs() - p_j.pz()) );
	  double coshdEta = cosh( eta_i - eta_j );
	  double cosdPhi = ( p_i.px()*p_j.px() + p_i.py()*p_j.py() ) / (pT_i*pT_j);
	  double kTdur = sqrt( pow(max(event[iPos].m(),event[jPos].m()),2) + 2.0*min(pow(pT_i,2),pow(pT_j,2)) * (coshdEta - cosdPhi) );
	  if (kTdur < kTmin)  kTmin = kTdur;
	}
      }
      //cout << event.size() << "  " << iSys << "  " << nPart << "   " << kTmin << endl;
      if (kTmin > pTshowerMax) {
	//cout << "VETO!" << endl;
	return true;
      }
    }
    //int ancestor = getLongLivedAncestry(i,pythia,longLivedIndices,finalPartonIndices);
    return false;   // Do not veto events that got this far.
  }
  */

  //virtual bool canVetoStep() {return true;}  // Allow a veto after (by default) first step.



private:

};
///


// find the closest parton to a given final-state particle
int getClosestParton(int ip, const Pythia & pythia, const vector<int> & finalPartonIndices)
{
  double DRmin = 1e10;
  double DRmaxAllowed = 0.5;  // max matching distance (controls high-mass outliers for final reconstructed particles)
  int closestParton = 0;
  for (unsigned int i = 0; i < finalPartonIndices.size(); i++) {
    double DR = REtaPhi(pythia.event[ip].p(),pythia.event[finalPartonIndices[i]].p());
    //cout << "      " << i << " (" << pythia.event[finalPartonIndices[i]].name() << "):  DR = " << DR << endl; 
    if (DR < DRmin && DR < DRmaxAllowed) { DRmin=DR; closestParton=finalPartonIndices[i]; }
  }
  return closestParton;
}




// Trace ancestry of final-state particles, in search of long-lived ancestors.
// 
// There is some subtlety when we want to pretend to extend the lifetime of otherwise prompt colored particles
// (such as RPV stops or gluinos).  In a full treatment, they would be hadronized before decaying.  But to keep
// things flexible -- in particular so that we can feed in fully-decayed events from MadGraph and don't need
// to invent a hadronization model on a case-by-case basis -- we hadronize them  here *after* they decay, 
// while they're still color-connected to the beam and/or to each other.  
// A given final-state hadron will then arise from a broken color string that stretches
// across the event.  The kinematic and color rearrangements here are quite complex, and difficult to 
// trace in detail.  Instead, here we use a workaround where we associate to each hadron an intermediate-state
// parton using DeltaR.  The intermediate partons are picked off of the event record after showering but before
// fragmentation.  In practice, for 2-body stop decays, 
// 
int getLongLivedAncestry(int ip, const Pythia & pythia, const vector<int> & longLivedIndices, const vector<int> & finalPartonIndices)
{
  //cout << endl << "start ancestry, id = " << pythia.event[ip].id() << endl;
  int iAncestor = ip;
  int nTries = 0;
  while (1) {
    int mo1 = pythia.event[iAncestor].mother1();
    int mo2 = pythia.event[iAncestor].mother2();
    if (mo1==0)  break;
    if (coloredLongLived && pythia.event[iAncestor].isHadron() && pythia.event[mo1].isParton() && pythia.event[mo2].isParton()) 
      iAncestor = getClosestParton(iAncestor,pythia,finalPartonIndices);
    else
      iAncestor = mo1;
    //cout << "  ancestor (" << iAncestor << "), id = " << pythia.event[iAncestor].id() << endl;
    for (unsigned int i = 0; i < longLivedIndices.size(); i++)  if (iAncestor==longLivedIndices[i])  return i+1;
    nTries++;  if(nTries > 1000)  break;  // avoid (rare) infinite loop
  }

  return 0;
}






//------------------------------------------------------------------------
//
int main(int argc, char ** argv) {

  // LHE file name
  string dir = ".";
  string lheFileName = "unweighted_events";
  string mass = "100";
  int nGen = 10000;
  string userTag = "";
  if (argc >= 2)    dir = argv[1];
  if (argc >= 3)    lheFileName = argv[2];
  if (argc >= 4)    mass = argv[3];
  if (argc >= 5)    nGen = atoi(argv[4]);
  if (argc >= 6)    userTag = argv[5];
  if (userTag != "")  userTag = "."+userTag;

  // Generator
  Pythia pythia(xmldir);
  pythiaPtr = &pythia;
  pythia.readString("PartonLevel:ISR = on"); 
  pythia.readString("PartonLevel:FSR = on"); 
  pythia.readString("PartonLevel:MPI = on"); 
  pythia.readString("HadronLevel:Hadronize = on"); 

  // Prevent "power shower" in BSM pair production events (much better agreement with matched samples this way)
  pythia.readString("SpaceShower:pTdampMatch = 1");

  // Test ME shower corrections off, if desired
  //pythia.readString("TimeShower:MEcorrections = off");////

  // Shower-level HT trigger and other vetos
  MyUserHooks* myUserHooks = new MyUserHooks();
  pythia.setUserHooksPtr(myUserHooks);

  // Set up the process
  // *****  If you want to reset any BR's relative to the SLHA file, do so after the pythia.init() call *****
  bool external = false;
  pythia.readString("Beams:idA = 2212");  // proton...
  pythia.readString("Beams:idB = 2212");  // ...on proton
  pythia.readString("Beams:eCM = 8000");  // LHC8
  float mass_float = atof(mass.c_str());
  if (lheFileName == "stop" || lheFileName == "stopHadron" || lheFileName == "stopHadron.DynamicalRPV") {
    idLongLived = 1000006;
    if (lheFileName == "stopHadron.DynamicalRPV")  pythia.readString("SLHA:file = "+slhafileDynamicalRPV);
    else                                           pythia.readString("SLHA:file = "+slhafileStop); 
    pythia.readString("SLHA:allowUserOverride = on");  // ...but allow us to override its parameters (v8175 onward)
    pythia.readString("SUSY:gg2squarkantisquark = on");
    pythia.readString("SUSY:qqbar2squarkantisquark = on");
    pythia.readString("SUSY:idA = 1000006");
    pythia.readString("SUSY:idB = 1000006");
    pythia.readString("1000006:m0 = "+mass);
    if (lheFileName == "stopHadron" || lheFileName == "stopHadron.DynamicalRPV") {
      idLongLived = 0;  // don't allow it to trace back to bare stops
      Rhadrons = true;
      pythia.readString("RHadrons:allow = on");
      pythia.readString("RHadrons:allowDecay = on");  // pythia will automatically take care of the stop decays "inside" the R-hadron
    }
  }
  else if (lheFileName == "RPVgluino" || lheFileName == "RPVgluinoHadron") {
    idLongLived = 1000021;
    pythia.readString("SLHA:file = "+slhafile); 
    pythia.readString("SLHA:allowUserOverride = on");  // ...but allow us to override its parameters (v8175 onward)
    pythia.readString("SUSY:gg2gluinogluino = on");
    pythia.readString("SUSY:qqbar2gluinogluino = on");
    pythia.readString("SUSY:idA = 1000021");
    pythia.readString("SUSY:idB = 1000021");
    pythia.readString("1000021:m0 = "+mass);
    if (lheFileName == "RPVgluinoHadron") {
      idLongLived = 0;  // don't allow it to trace back to bare gluinos
      Rhadrons = true;
      pythia.readString("RHadrons:allow = on");
      pythia.readString("RHadrons:allowDecay = on");  // pythia will automatically take care of the stop decays "inside" the R-hadron
    }
  }
  else if (lheFileName == "MiniSplitGluino.lightLSP" || lheFileName == "MiniSplitGluino.heavyLSP" || 
	   lheFileName == "MiniSplitGluinoHadron.lightLSP" || lheFileName == "MiniSplitGluinoHadron.heavyLSP") {
    idLongLived = 1000021;
    pythia.readString("SLHA:file = "+slhafileMiniSplit); 
    pythia.readString("SLHA:allowUserOverride = on");  // ...but allow us to override its parameters (v8175 onward)
    pythia.readString("SUSY:gg2gluinogluino = on");
    pythia.readString("SUSY:qqbar2gluinogluino = on");
    pythia.readString("SUSY:idA = 1000021");
    pythia.readString("SUSY:idB = 1000021");
    pythia.readString("1000021:m0 = "+mass);
    if (lheFileName == "MiniSplitGluino.lightLSP" || lheFileName == "MiniSplitGluinoHadron.lightLSP")   pythia.readString("1000022:m0 = 0");  // massless neutralino LSP
    if (lheFileName == "MiniSplitGluino.heavyLSP" || lheFileName == "MiniSplitGluinoHadron.heavyLSP") {
      float neutralinoMass_float = mass_float-100;
      stringstream buffer;  buffer << neutralinoMass_float;  string neutralinoMass(buffer.str());
      pythia.readString("1000022:m0 = "+neutralinoMass);
    }
    if (lheFileName == "MiniSplitGluinoHadron.lightLSP" || lheFileName == "MiniSplitGluinoHadron.heavyLSP") {
      idLongLived = 0;  // don't allow it to trace back to bare gluinos
      Rhadrons = true;
      pythia.readString("RHadrons:allow = on");
      pythia.readString("RHadrons:allowDecay = on");  // pythia will automatically take care of the stop decays "inside" the R-hadron
    }
  }
  else if (lheFileName == "GMSBHiggsino.Z" || lheFileName == "GMSBHiggsino.h" || lheFileName == "GMSBHiggsino.Z+h") {
    idLongLived = 1000025;  // neutralino3 assumed to be lightest Higgsino-like neutralino
    if (lheFileName == "GMSBHiggsino.Z")  pythia.readString("SLHA:file = "+slhafileGMSBHiggsinoZ); 
    if (lheFileName == "GMSBHiggsino.h")  pythia.readString("SLHA:file = "+slhafileGMSBHiggsinoh);     
    if (lheFileName == "GMSBHiggsino.Z+h")  pythia.readString("SLHA:file = "+slhafileGMSBHiggsinoZ); // read either Z or h file, set correct BR's below init()  
    pythia.readString("SLHA:allowUserOverride = on");  // ...but allow us to override its parameters (v8175 onward)
    pythia.readString("SUSY:qqbar2chi0chi0 = on");
    pythia.readString("SUSY:qqbar2chi+-chi0 = on");
    pythia.readString("SUSY:qqbar2chi+chi- = on");
    pythia.readString("Higgs:useBSM = off");
    pythia.readString("1000022:m0 = 0");  // massless bino-like neutralino1 LSP (fake gravitino)
    pythia.readString("1000023:m0 = 100000");  // decouple wino-like neutralino2
    pythia.readString("1000024:m0 = 100000");  // decouple wino-like chargino1
    float heavierHiggsinoMasses_float = mass_float+1.0;
    stringstream buffer;  buffer << heavierHiggsinoMasses_float;  string heavierHiggsinoMasses(buffer.str());
    pythia.readString("1000025:m0 = "+mass);   // lightest Higgsino
    pythia.readString("1000037:m0 = "+heavierHiggsinoMasses);
    pythia.readString("1000035:m0 = "+heavierHiggsinoMasses);
  }
  else if (lheFileName == "RPVHiggsino") {
    idLongLived = 1000025;  // neutralino3 assumed to be lightest Higgsino-like neutralino
    pythia.readString("SLHA:file = "+slhafileRPVHiggsino); 
    pythia.readString("SLHA:allowUserOverride = on");  // ...but allow us to override its parameters (v8175 onward)
    pythia.readString("SUSY:qqbar2chi0chi0 = on");
    pythia.readString("SUSY:qqbar2chi+-chi0 = on");
    pythia.readString("SUSY:qqbar2chi+chi- = on");
    pythia.readString("Higgs:useBSM = off");
    pythia.readString("1000022:m0 = 0");  // massless bino-like neutralino1 LSP (fake gravitino)
    pythia.readString("1000023:m0 = 100000");  // decouple wino-like neutralino2
    pythia.readString("1000024:m0 = 100000");  // decouple wino-like chargino1
    float heavierHiggsinoMasses_float = mass_float+1.0;
    stringstream buffer;  buffer << heavierHiggsinoMasses_float;  string heavierHiggsinoMasses(buffer.str());
    pythia.readString("1000025:m0 = "+mass);   // lightest Higgsino
    pythia.readString("1000037:m0 = "+heavierHiggsinoMasses);
    pythia.readString("1000035:m0 = "+heavierHiggsinoMasses);
  }
  else if (lheFileName == "GMSBgluino") {
    idLongLived = 1000021;
    pythia.readString("SLHA:file = "+slhafileGMSBsquarkgluino); 
    pythia.readString("SLHA:allowUserOverride = on");  // ...but allow us to override its parameters (v8175 onward)
    pythia.readString("SUSY:gg2gluinogluino = on");
    pythia.readString("SUSY:qqbar2gluinogluino = on");
    pythia.readString("SUSY:idA = 1000021");
    pythia.readString("SUSY:idB = 1000021");
    pythia.readString("1000002:m0 = 100000");  // decouple up squark
    pythia.readString("2000002:m0 = 100000");  // decouple up squark
    pythia.readString("1000021:m0 = "+mass);   // gluino mass
    //  NOTE:  At least in Pythia 8.176, this R-hadronization doesn't work with this decay mode.  So have to run "bare" and attempt hadron->parton mapping.
  }
  else if (lheFileName == "GMSBsquark") {
    idLongLived = 1000002;
    pythia.readString("SLHA:file = "+slhafileGMSBsquarkgluino); 
    pythia.readString("SLHA:allowUserOverride = on");  // ...but allow us to override its parameters (v8175 onward)
    pythia.readString("SUSY:gg2squarkantisquark = on");
    pythia.readString("SUSY:qqbar2squarkantisquark = on");
    pythia.readString("SUSY:idA = 1000002");
    pythia.readString("SUSY:idB = 1000002");
    pythia.readString("1000002:m0 = "+mass);  // up squark mass
    pythia.readString("1000021:m0 = 100000");   // decouple gluino
    //  NOTE:  At least in Pythia 8.176, this R-hadronization doesn't work with this decay mode.  So have to run "bare" and attempt hadron->parton mapping.
  }
  else if (lheFileName == "GMSBsquark.gravToNeu") {   // replace gravitino with "massless" neutralino, so that ME corrections to the shower kick in
    idLongLived = 1000002;
    pythia.readString("SLHA:file = /Users/brock/Displaced/slha_input.GMSBsquark.gravToNeu.spc"); 
    pythia.readString("SLHA:allowUserOverride = on");  // ...but allow us to override its parameters (v8175 onward)
    pythia.readString("SUSY:gg2squarkantisquark = on");
    pythia.readString("SUSY:qqbar2squarkantisquark = on");
    pythia.readString("SUSY:idA = 1000002");
    pythia.readString("SUSY:idB = 1000002");
    pythia.readString("1000002:m0 = "+mass);  // up squark mass
    pythia.readString("1000022:m0 = 0.001");  // neutralino mass
    pythia.readString("1000021:m0 = 100000");   // decouple gluino
    //  NOTE:  At least in Pythia 8.176, this R-hadronization doesn't work with this decay mode.  So have to run "bare" and attempt hadron->parton mapping.
  }
  else if (lheFileName == "ATLASneutralinoL" || lheFileName == "ATLASneutralinoH") {
    idLongLived = 1000022;
    pythia.readString("SLHA:file = "+slhafileATLASneutralino); 
    pythia.readString("SLHA:allowUserOverride = on");  // ...but allow us to override its parameters (v8175 onward)
    pythia.readString("SUSY:gg2squarkantisquark = on");
    pythia.readString("SUSY:qqbar2squarkantisquark = on");
    pythia.readString("SUSY:qq2squarksquark:onlyQCD = on");  // neutralino/chargino exchange ignored
    pythia.readString("1000001:m0 = "+mass);
    pythia.readString("1000002:m0 = "+mass);
    pythia.readString("1000003:m0 = "+mass);
    pythia.readString("1000004:m0 = "+mass);
    pythia.readString("2000001:m0 = "+mass);
    pythia.readString("2000002:m0 = "+mass);
    pythia.readString("2000003:m0 = "+mass);
    pythia.readString("2000004:m0 = "+mass);
    if (lheFileName == "ATLASneutralinoH")  pythia.readString("1000022:m0 = 494");  // ATLAS "high" mass neutralino
    if (lheFileName == "ATLASneutralinoL")  pythia.readString("1000022:m0 = 108");  // ATLAS "low" mass neutralino
  }
  else if (lheFileName == "DisplacedSUSY") {
    idLongLived = 0;  // automatically use stop-hadrons...don't allow it to trace back to bare stops
    Rhadrons = true;
    pythia.readString("RHadrons:allow = on");
    pythia.readString("RHadrons:allowDecay = on");  // pythia will automatically take care of the stop decays "inside" the R-hadron
    pythia.readString("SLHA:file = "+slhafileDisplacedSUSY);
    pythia.readString("SLHA:allowUserOverride = on");  // ...but allow us to override its parameters (v8175 onward)
    pythia.readString("SUSY:gg2squarkantisquark = on");
    pythia.readString("SUSY:qqbar2squarkantisquark = on");
    pythia.readString("SUSY:idA = 1000006");
    pythia.readString("SUSY:idB = 1000006");
    pythia.readString("1000006:m0 = "+mass);
  }
  else {
    // external Les Houches Event file
    external = true;
    pythia.readString("Beams:frameType = 4"); 
    string lheTag = "Beams:LHEF = "+dir+"/"+lheFileName+".lhe";
    pythia.readString(lheTag); 
  }

  // Initialize Pythia
  pythia.init();

  // Special BR reset for mixed Z+h Higgsino decays, assuming large tan\beta limit
  if (lheFileName == "GMSBHiggsino.Z+h") {
    double GammaZ = 0;  double Gammah = 0;
    double BRZ = 0;  double BRh = 0;
    double mz = 91.2;  double mh = 125;
    if (mass_float > mz)  GammaZ = pow( 1-mz*mz/mass_float/mass_float, 4.0 );
    if (mass_float > mh)  Gammah = pow( 1-mh*mh/mass_float/mass_float, 4.0 );
    if (GammaZ > 0 || Gammah > 0) {
      BRZ = GammaZ / (GammaZ+Gammah);
      BRh = Gammah / (GammaZ+Gammah);
    }
    stringstream bufferZ;  bufferZ << BRZ;  string BRZ_string(bufferZ.str());
    stringstream bufferh;  bufferh << BRh;  string BRh_string(bufferh.str());
    pythia.readString("1000025:0:bRatio = "+BRZ_string);
    pythia.readString("1000025:1:bRatio = "+BRh_string);  
    pythia.particleData.list(1000025);
  }
 

  // Open "output.dat" file
  ofstream outputFile;
  if (external)  outputFile.open((dir+"/output."+lheFileName+userTag+".dat").c_str());
  else           outputFile.open((dir+"/output."+lheFileName+userTag+".m"+mass+".dat").c_str());
 
  // Cross section, event counters
  double sigma = 0;
  int nEventsGenerated = 0;
  int nEventsPass = 0;

  // Allow for possibility of a few faulty events.
  int nAbort = 1000;
  int iAbort = 0;

  // Special counters to check Pythia treatment of R-hadron charges
  int nRhadronsProduced = 0;
  int nChargedRhadronsProduced = 0;

  // Analysis histogram (use if desired)
  Hist hist("up quark energy spectrum in gluino rest frame", 20, 0., atof(mass.c_str())/2);
  Hist hist_beta("heavy particle velocity spectrum", 100, 0.,1.);
  

  // Begin event loop; generate until none left in input file.     
  for (int iEvent = 0; ; ++iEvent) {

    if (!external && iEvent >= nGen)  break;

    //cout << iEvent << endl;
    //if (iEvent >= 10000)  break;

    // Generate events, and check whether generation failed.
    if (!pythia.next() || iEvent >= nHadronizeMax) {
      
      // If failure because reached end of file then exit event loop.
      if (pythia.info.atEndOfFile()) break; 

      // First few failures write off as "acceptable" errors, then quit.
      if (++iAbort < nAbort) continue;
      cout << "TOO MANY ERRORS:  BAILING OUT!" << endl;
      break;
    }


    nEventsGenerated++;
    nEventsPass++;

    // Particle holders
    vector<Particle> hardEventParticles;
    vector<Particle> HFhadrons;
    vector<Particle> finalStateParticles;
    vector<int>  finalStateParticleIndices;  // for debugging
    
    // What particle shall we set long-lived?
    int idLongLived_ = idLongLived;  // check global flag
    if (idLongLived_ == 0 && !Rhadrons && pythia.process.size() > 7) {  // if 0, decide who to keep stable on-the-fly based on hard event content
      bool isResonant = (pythia.process[6].mother1() == 5 && pythia.process[7].mother1() == 5);
      if ( isResonant && abs(pythia.process[6].id()) == abs(pythia.process[7].id()))  idLongLived_ = abs(pythia.process[6].id());
      if (!isResonant && abs(pythia.process[5].id()) == abs(pythia.process[6].id()))  idLongLived_ = abs(pythia.process[5].id());
      if (!isResonant && abs(pythia.process[5].id()) != abs(pythia.process[6].id()))  idLongLived_ = abs(pythia.process[5].id());  
    }
    //cout << idLongLived_ << endl;


    // Extract particles from the hard event
    for (int i = 0; i < pythia.process.size(); ++i) { 

      Particle part = pythia.process[i];

      // optional:  fill analysis histogram (energy of up-type quark in RPV gluino frame, to check if 3-body phase space is flat (linear rise in E))
      if (part.status()==23 && abs(part.id()==2)) {
	Vec4 moVec = pythia.process[part.mother1()].p();
	////hist.fill(part.p()*moVec/moVec.mCalc());
      }

      // pythia codes:  21: initial-state,  22: intermediate hard particle from user process,  23:  "final" hard particle from user process
      // "-" sign indicates particle has been decayed or annihilated
      bool hard = false;
      if (part.status() == -21) { 
	hard = true;  
	part.statusCode(1);   // will retain sign, setting status to -1, miming LHE conventions from MadGraph
      } 
      if (part.status() == -22) { 
	hard = true;  
	part.statusPos(); 
	part.statusCode(2);   // also miming LHE, for primary unstable particle
      } 
      if (part.status() == 23)  {
	hard = true;  
	int mid = pythia.process[part.mother1()].id();
	if (mid<0)  part.statusNeg();  else part.statusPos();
	part.statusCode(abs(mid));  // departing from LHE convention, set the status to the mother ID
      }
      if (hard && part.status() != 90 && abs(part.status()) < 3e6) {  // status cutoff removes rare (and mysterious) glitches
	hardEventParticles.push_back(part);
	if (abs(part.id())==idLongLived && fabs(part.p().eta()) < 10.0)  hist_beta.fill(part.p().pAbs()/part.p().e());
	continue;
      }
    }

    // First pass through the event-record:  Identify our long-lived particles immediately before they decay (e.g., after any showering),
    //  and assign their codes based on order of appearance.  Also build up a list of "post-shower, pre-fragmentation" partons, for
    //  matching to final-state hadrons if the long-lived particle isn't color-singlet.
    vector<int> longLivedIndices;
    vector<int> finalPartonIndices;
    for (int i = 1; i < pythia.event.size(); ++i) { 
      Particle part = pythia.event[i];
      if (abs(part.status()) < 70 && (abs(pythia.event[part.daughter1()].status()) > 70 || part.daughter1() == 0)) {
	finalPartonIndices.push_back(i);  // post-shower / pre-fragmentation parton
      }
      if ( abs(part.id()) != idLongLived_ && !(Rhadrons && isRhadron(part.id())) )  continue;
      bool final = true;
      for (int j = part.daughter1(); j <= part.daughter2(); j++)  
	if (abs(pythia.event[j].id())==idLongLived_ || (Rhadrons && isRhadron(pythia.event[j].id()))) { final=false; break; }
      if (!final) continue;
      longLivedIndices.push_back(i);
      if (part.colType() != 0)  coloredLongLived = true;  // catch if our would-be long-lived particles are actually colored...requires careful hadron->parton mapping
      if (isRhadron(part.id())) {  nRhadronsProduced++;  if (part.isCharged() != 0) nChargedRhadronsProduced++; }
    }

    // Extract final-state particles and b/c-hadrons
    vector<Vec4> rebuiltLongLived;  for (unsigned int i = 0; i < longLivedIndices.size(); i++)  rebuiltLongLived.push_back(Vec4(0,0,0,0));
    for (int i = 1; i < pythia.event.size(); ++i) { 

      Particle part = pythia.event[i];

      // final-state particles
      if (part.isFinal())  {
	int ancestor = getLongLivedAncestry(i,pythia,longLivedIndices,finalPartonIndices);
	part.statusCode(ancestor);  // departing from LHE convention, set the status to the ancestry ID
	if (ancestor > 0)  rebuiltLongLived[ancestor-1] += part.p();
	////if (ancestor > 0)  cout << ancestor << endl;
	finalStateParticles.push_back(part);
	finalStateParticleIndices.push_back(i);
	continue;
      }

      // b and c hadrons
      bool isHF = false;
      int flavor = getFlavor(part.id());
      if ((flavor == 5 || flavor == 4) && !part.isFinal()) {
	//cout << endl << "HF-HADRON (" << i << "):   " << part.id() << endl;
	//for (int j = part.daughter1(); j <= part.daughter2(); j++)  cout << "    daughter (" << j << ") = " << pythia.event[j].id() << endl;

	// get heaviest flavor daughter
	int flavorDaughter = -1;
	for (int j = part.daughter1(); j <= part.daughter2(); j++) {
	  int thisFlavor = getFlavor(pythia.event[j].id());
	  if (thisFlavor > flavorDaughter)  flavorDaughter = thisFlavor; 
	}

	// get heaviest flavor ancestor
	int flavorAncestor = -1;
	int iAncestor = i;
	while (1) {
	  iAncestor = pythia.event[iAncestor].mother1();
	  if (iAncestor == 0)  break;
	  //cout << " ancestor (" << iAncestor << "), id = " << pythia.event[iAncestor].id() << endl;
	  int thisFlavor = getFlavor(pythia.event[iAncestor].id());
	  if (thisFlavor > flavorAncestor)  flavorAncestor = thisFlavor;
	  if (thisFlavor > flavor)  break;
	}
      
	// b hadrons immediately before decay, and c hadrons not from b decay
	if (flavorDaughter < flavor && (flavor == 5 || flavorAncestor <= flavor))   isHF = true;

	//cout << "   daughter flavor = " << flavorDaughter << endl;
	//cout << "   ancestor flavor = " << flavorAncestor << endl;
	//cout << "   assigned stat   = " << outStat << endl << endl;
      }
      if (isHF)  {
	HFhadrons.push_back(part);
	continue;
      }
      
    }
    
    /*
    // check that we succesfully captured the decay products
    // (be mindful of "displaced" colored decays to hadrons!)
    for (unsigned int i = 0; i < longLivedIndices.size(); i++) {
      cout << rebuiltLongLived[i];
      //cout << rebuiltLongLived[i] - pythia.event[longLivedIndices[i]].p();
      //cout << rebuiltLongLived[i].mCalc() - pythia.event[longLivedIndices[i]].p().mCalc() << endl;
    }
    cout << endl;
    */
    
    //pythia.process.list();//////
    //pythia.event.list();////////
     
    // Output to text file
    outputFile << fixed << setprecision(2);
    outputFile << iEvent << " " << hardEventParticles.size() << endl;
    for (unsigned int i = 0; i < hardEventParticles.size(); i++) {
      Particle part = hardEventParticles[i];
      outputFile << part.id() << "  " << part.px() << " " << part.py() << " " << part.pz() << " " << part.e() << "  " << part.status() << endl;
    }
    outputFile << "0 " << HFhadrons.size() << endl;
    for (unsigned int i = 0; i < HFhadrons.size(); i++) { 
      Particle part = HFhadrons[i];
      outputFile << part.id() << "  " << part.px() << " " << part.py() << " " << part.pz() << " " << part.e() << endl;
    }
    outputFile << "0 " << finalStateParticles.size() << endl;
    for (unsigned int i = 0; i < finalStateParticles.size(); i++) { 
      Particle part = finalStateParticles[i];
      outputFile << part.id() << "  " << part.px() << " " << part.py() << " " << part.pz() << " " << part.e() << "  " << part.status() << endl;
      /////outputFile << finalStateParticleIndices[i] << "  " << part.id() << "  " << part.px() << " " << part.py() << " " << part.pz() << " " << part.e() << "  " << part.status() << endl;
    }    
    outputFile << endl;
    

  // End of event loop.        
  }                                           

  
  // Write "sigma.dat" file
  sigma =  pythia.info.sigmaGen()*1e12;
  ofstream sigmaFile;
  if (external)  sigmaFile.open((dir+"/sigma."+lheFileName+userTag+".dat").c_str());
  else           sigmaFile.open((dir+"/sigma."+lheFileName+userTag+".m"+mass+".dat").c_str());
  sigmaFile << sigma << " " << nEventsGenerated << " " << nEventsPass << endl;

  pythia.stat();

  // Optionally, write stats on R-hadrons
  cout << endl;
  cout << "# R-hadrons     : " << nRhadronsProduced << endl;
  cout << "fraction charged: " << nChargedRhadronsProduced*1./nRhadronsProduced << endl;

  // Optionally, display analysis histo
  //cout << hist;
  cout << hist_beta;

  // Done.                           
  return 0;
}
